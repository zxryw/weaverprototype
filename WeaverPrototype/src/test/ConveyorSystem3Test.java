package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

import weaver.FileIO;
import weaver.Scope;

public class ConveyorSystem3Test {
	
	private Scope scp ;
	private FileIO fileio;
	
	private String modelicaLibPattern = "Modelica[.[a-zA-Z_]+]+";
	
	private String modelicanowpath = "C:\\modelica\\Now\\";
	private String modelicalibpath = "C:\\OMDev\\tools\\msys\\home\\watanabe\\OpenModelica\\build\\lib\\omlibrary\\Modelica 3.2.2";
	

	@Before
	public void setUp() throws Exception {
		scp = new Scope();
		fileio = new FileIO();
	}
	
	public void check(ArrayList<String> arrList) {
		for(int i=0;i<arrList.size();i++) {
			System.out.println(arrList.get(i));
		}
	}
	
	
	@Test
	public void test1() {
		ArrayList<String> srchSyntax = new ArrayList<String>();
//		srchSyntax.add("Modelica.Blocks.Math.Feedback:stepVoltage1:  replace(signalVoltage)annotation(PointCut=\"Modelica.Blocks.Math.Feedback stepVoltage1\");");
//		srchSyntax.add("Modelica.Blocks.Continuous.Integrator:stepVoltage1:  replace(signalVoltage)annotation(PointCut=\"Modelica.Blocks.Continuous.Integrator stepVoltage1\");");
//		srchSyntax.add("Modelica.Blocks.Math.Add:stepVoltage1:  replace(signalVoltage)annotation(PointCut=\"Modelica.Blocks.Math.Add stepVoltage1\");");
//		srchSyntax.add("MortorBlockNONCAUSAL:stepVoltage1:  replace(signalVoltage)annotation(PointCut=\"MortorBlockNONCAUSAL stepVoltage1\");");
//		srchSyntax.add("Modelica.Blocks.Math.Feedback:mortor.flange_b:  link(angleSensor.flange)annotation(PointCut=\"Modelica.Blocks.Math.Feedback mortor.flange_b\");");
//		srchSyntax.add("Modelica.Blocks.Continuous.Integrator:mortor.flange_b:  link(angleSensor.flange)annotation(PointCut=\"Modelica.Blocks.Continuous.Integrator mortor.flange_b\");");
//		srchSyntax.add("Modelica.Blocks.Math.Add:mortor.flange_b:  link(angleSensor.flange)annotation(PointCut=\"Modelica.Blocks.Math.Add mortor.flange_b\");");
//		srchSyntax.add("MortorBlockNONCAUSAL:mortor.flange_b:  link(angleSensor.flange)annotation(PointCut=\"MortorBlockNONCAUSAL mortor.flange_b\");");
//		srchSyntax.add("MortorBlockNONCAUSAL:mortor.flange_b:  link(speedSensor.flange)annotation(PointCut=\"MortorBlockNONCAUSAL mortor.flange_b\");");
		ArrayList<String> srch = new ArrayList<String>();
		
		for(String path :srchSyntax) {
			srch.addAll(scp.inputModel(path));
		}
		//check(srch);
		
	}
	
	@Test
	public void test2() {
		ArrayList<String> srchSyntax = new ArrayList<String>();
		srchSyntax.add("ConveyorSystem3Controll:feedback1.u1:  input(conveyor.u)annotation(PointCut=\"ConveyorSystem3Controll feedback1.u1\");");
		//srchSyntax.add("ConveyorSystem3Controll:sum.y:  output(conveyor.y)annotation(PointCut=\"ConveyorSystem3Controll sum.y\");");
		ArrayList<String> srch = new ArrayList<String>();
		
		for(String path :srchSyntax) {
			srch.addAll(scp.inputModel(path));
		}
		check(srch);
	}

}
