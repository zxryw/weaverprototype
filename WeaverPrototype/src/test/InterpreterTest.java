package test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import weaver.FileIO;
import weaver.Interpreter;
import weaver.Scope;

/**
 * @author watanabe
 * 注意事項
 * アスペクトファイルを取り出すfor文の中では一つ一つを構文チェック構文生成しているのでArrayListにaddALlせずに代入だけしてしまうと上書きされてしまうので注意
 * 構文チェックの結果を残したければaddALL
 *
 */
public class InterpreterTest {
	
	
	//インスタンス
	private Scope scp;
	private FileIO fileio;
	private Interpreter interpret;
	
	//パス
	private String modelicablockpath= "C:\\modelica\\blockdiagram\\";
	
	//わかりやすい正規表現！！
	private String division = "**********************************************************************************************************************************";
	private String aspectPattern ="aspect\\s[a-zA-Z0-9]*";
	private String annotationPattern = "annotation\\(Aspect=\"[a-zA-Z]*\"\\);";
	private String pointcutPattern = "[[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\s[[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_]+";
	private String advicePattern ="\\s*[a-z]*\\([[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\)annotation\\(PointCut=\""+pointcutPattern +"\"\\);";
	
	@Before
	public void setUp() throws Exception {
		this.scp = new Scope();
		this.fileio = new FileIO();
		this.interpret = new Interpreter();
	}
	
	public void check(ArrayList<String> arrList) {
		for(int i=0;i<arrList.size();i++) {
			System.out.println(arrList.get(i));
		}
	}
	
	/**
	 * 正規表現試験場
	 */
	@Test
	public void regexTest() {
		assert("  replace(signalVoltage)annotation(PointCut=\"Modelica.DCmortorNONCAUSAL stepVoltage.flange\");".matches
				("\\s*replace\\([a-zA-Z0-9]*\\)annotation\\(PointCut=\"[a-zA-Z0-9\\.]*\\s[a-zA-Z0-9\\.]*\"\\);"));
		
		assert("  replace(signalVoltage)annotation(PointCut=\"Modelica.Electrical.DCmortorNONCAUSAL stepVoltage.flange.ssda.sdaA.as\");".matches
				("\\s*replace\\([a-zA-Z0-9]*\\)annotation\\(PointCut=\"[[a-zA-Z0-9]*.]*\\s[[a-zA-Z0-9]*.]*\"\\);"));
		
		assert("  replace(signalVoltage)annotation(PointCut=\"Modelica.Electrical.DCmortorNONCAUSAL stepVoltage.flange.ssda.sdaA.s\");".matches
				("\\s*replace\\([a-zA-Z0-9_]*\\)annotation\\(PointCut=\"[[a-zA-Z0-9_]*.]*[a-zA-Z0-9_]+\\s[[a-zA-Z0-9_]*.]*[a-zA-Z0-9_]+\"\\);"));
		
		assert("  replace(signalVoltage)annotation(PointCut=\"DCmortorNONCAUSAL stepVoltage\");").matches
				("\\s*replace\\([a-zA-Z0-9_]*\\)annotation\\(PointCut=\""
						+ pointcutPattern
						+ "\"\\);");
				
		assert("  replace(signalVoltage)annotation(PointCut=\"DCmortorNONCAUSAL stepVoltage\");").matches
				("\\s*[a-z]*\\([a-zA-Z0-9_]*\\)annotation\\(PointCut=\""
						+ pointcutPattern
						+ "\"\\);");
		
		String s = "s";
		assert("ss".matches(s + "s"));
		
		//System.out.println("*.a.a.a.stepVoltage".matches("[[a-zA-Z0-9\\*]*.]*[a-zA-Z0-9\\*]*"));
	}
	
	
	/**
	 * 読み込んだアスペクトファイルをインタプリタ―クラスのメソッドに渡しaspect〇〇とルール通りに記述がなされているか調べるテスト
	 */
	@Test
	public void researchAspectRule() {
		ArrayList<String> aspects = scp.findFile(modelicablockpath);
		ArrayList<String> rightAspect = new ArrayList<String>();
		for(int i=0;i<aspects.size();i++) {
			String srchAspect = aspects.get(i);
			ArrayList<String> aspect = fileio.input(modelicablockpath+srchAspect);
			ArrayList<String> resultAspect = interpret.fileCheck(aspect,aspectPattern);
			rightAspect.addAll(resultAspect);
			if(resultAspect.size()!=0)rightAspect.add(division);
		}
		//check(rightAspect);
		assertThat(rightAspect.get(0),is("aspect BlockDiagramAspect2"));
	}
	
	/**
	 * 読み込んだアスペクトファイル群の単独アノテーションがルール通りに記述されているか判断できるか調べるテスト
	 */
	@Test
	public void searchAnnotationWithInterpreter() {
		ArrayList<String> aspects = scp.findFile(modelicablockpath);
		ArrayList<String> rightAspect = new ArrayList<String>();
		for(int i=0;i<aspects.size();i++) {
			String srchAspect = aspects.get(i);
			ArrayList<String> inputAspect = fileio.input(modelicablockpath+srchAspect);
			ArrayList<String> resultAspect = interpret.fileCheck(inputAspect,aspectPattern);
			resultAspect = interpret.fileCheck(resultAspect,annotationPattern);
			rightAspect.addAll(resultAspect);
			if(resultAspect.size()!=0)rightAspect.add(division);
		}
		//check(rightAspect);
		assertThat(rightAspect.get(1),is("aspect BlockDiagramAspect3//aasdad"));
	}
	
	/**
	 * 単独アノテーションにおいて接続アスペクトなのか変換アスペクトなのか解釈させる
	 * 最終的に二つのarrayListに格納されていると嬉しい
	 */
	@Test
	public void interpretAnnotation() {
		ArrayList<String> aspects = scp.findFile(modelicablockpath);
		ArrayList<String> convertAspect = new ArrayList<String>();
		ArrayList<String> retrofitAspect = new ArrayList<String>();
		for(int i=0;i<aspects.size();i++) {
			String srchAspect = aspects.get(i);
			ArrayList<String> inputAspect = fileio.input(modelicablockpath+srchAspect);
			ArrayList<String> resultAspect = interpret.fileCheck(inputAspect,aspectPattern);
			resultAspect = interpret.fileCheck(resultAspect,annotationPattern);
			convertAspect.addAll(interpret.interpretAnnotationAspectType(resultAspect,"ConvertAspect"));
			//convertAspect.add(division);二つ以上の変換アスペクトがあったときのための区切り
			retrofitAspect.addAll( interpret.interpretAnnotationAspectType(resultAspect,"RetrofitAspect"));
			//retrofitAspect.add(division);二つ以上の変換アスペクトがあったときのための区切り
		}
		
		//check(convertAspect);
		//System.out.println("上が変換アスペクトで下が組み込みアスペクトです");
		//check(retrofitAspect);
		assertThat(retrofitAspect.get(1),is("aspect BlockDiagramAspect3//aasdad"));
		assertThat(convertAspect.get(0),is("aspect DCmotorAspect5"));
	}
	
	/**
	 * adviceセクションを判断させる
	 */
	@Test
	public void searchAdvice() {
		ArrayList<String> aspects = scp.findFile(modelicablockpath);
		ArrayList<String> convertAspect = new ArrayList<String>();
		ArrayList<String> retrofitAspect = new ArrayList<String>();
		for(int i=0;i<aspects.size();i++) {
			String srchAspect = aspects.get(i);
			ArrayList<String> inputAspect = fileio.input(modelicablockpath+srchAspect);
			ArrayList<String> resultAspect = interpret.fileCheck(inputAspect,aspectPattern);
			resultAspect = interpret.fileCheck(resultAspect,annotationPattern);
			convertAspect.addAll(interpret.interpretAnnotationAspectType(resultAspect,"ConvertAspect"));
			convertAspect = interpret.fileCheck(convertAspect, "advice");
			retrofitAspect.addAll( interpret.interpretAnnotationAspectType(resultAspect,"RetrofitAspect"));
			retrofitAspect = interpret.fileCheck(retrofitAspect, "advice");
		}
//		check(convertAspect);
//		System.out.println("上が変換アスペクトで下が組み込みアスペクトです"+division);
//		check(retrofitAspect);
		assertThat(retrofitAspect.get(1),is("aspect BlockDiagramAspect3//aasdad"));
		assertThat(convertAspect.get(0),is("aspect DCmotorAspect5"));
	}
	
	/**
	 * adviceセクションを解釈させる
	 */
	@Test
	public void interpretAdviceSection() {
		ArrayList<String> aspects = scp.findFile(modelicablockpath);
		ArrayList<String> convertAspect = new ArrayList<String>();
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAspect = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(int i=0;i<aspects.size();i++) {
			String srchAspect = aspects.get(i);
			ArrayList<String> inputAspect = fileio.input(modelicablockpath+srchAspect);
			ArrayList<String> resultAspect = interpret.fileCheck(inputAspect,"aspect\\s[a-zA-Z0-9]*");
			resultAspect = interpret.fileCheck(resultAspect,"annotation\\(Aspect=\"[a-zA-Z]*\"\\);");
			convertAspect.addAll(interpret.interpretAnnotationAspectType(resultAspect,"ConvertAspect"));//convertAspectが複数あったときうまくまとめれない
			//複数のアスペクトが存在した場合それを一つのアスペクトファイルにまとめるメソッドがあったほうがいいかも
			convertAspect = interpret.fileCheck(convertAspect, "advice");
			convertAdvice = interpret.interpretAdviceSection(convertAspect);
			
			retrofitAspect.addAll( interpret.interpretAnnotationAspectType(resultAspect,"RetrofitAspect"));
			retrofitAspect = interpret.fileCheck(retrofitAspect, "advice");
			retrofitAdvice=interpret.interpretAdviceSection(retrofitAspect);
		}
//		check(convertAdvice);
//		System.out.println(division);
//		check(retrofitAdvice);
		
		assertThat(convertAdvice.get(0),is("  replace(signalVoltage)annotation(PointCut=\"DCmortorNONCAUSAL stepVoltage1\");"));
		assertThat(retrofitAdvice.get(0),is("  input(dcmotor.u)annotation(PointCut=\"DCmotorControll feedback1.u2\");"));
	}
	
	/**
	 * アドバイス文の構文が正しいか判断させる
	 */
	@Test
	public void interpretAdviceSection2() {
		ArrayList<String> aspects = scp.findFile(modelicablockpath);
		ArrayList<String> convertAspect = new ArrayList<String>();
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAspect = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(int i=0;i<aspects.size();i++) {
			String srchAspect = aspects.get(i);
			ArrayList<String> inputAspect = fileio.input(modelicablockpath+srchAspect);
			ArrayList<String> resultAspect = interpret.fileCheck(inputAspect,aspectPattern);
			resultAspect = interpret.fileCheck(resultAspect,annotationPattern);
			convertAspect = interpret.interpretAnnotationAspectType(resultAspect,"ConvertAspect");
			convertAspect = interpret.fileCheck(convertAspect, "advice");
			convertAdvice.addAll(interpret.interpretAdviceSection(convertAspect));
			convertAdvice = interpret.fileCheck(convertAdvice,advicePattern);
			
			
			retrofitAspect.addAll( interpret.interpretAnnotationAspectType(resultAspect,"RetrofitAspect"));
			retrofitAspect = interpret.fileCheck(retrofitAspect, "advice");
			retrofitAdvice=interpret.interpretAdviceSection(retrofitAspect);
			retrofitAdvice = interpret.fileCheck(retrofitAdvice,advicePattern);
		}
		assertThat(convertAdvice.get(0),is("  replace(signalVoltage)annotation(PointCut=\"DCmortorNONCAUSAL stepVoltage1\");"));
		assertThat(retrofitAdvice.get(0),is("  input(dcmotor.u)annotation(PointCut=\"DCmotorControll feedback1.u2\");"));
		
	}
	
	/**
	 * adviceSectionチェスト
	 */
	@Test
	public void interpretAdviceSectionTest() {
		ArrayList<String> aspects = scp.findFile(modelicablockpath);
		
		ArrayList<String> convertAspect = new ArrayList<String>();
		ArrayList<String> convertAdviceSection = new ArrayList<String>();
		
		ArrayList<String> retrofitAspect = new ArrayList<String>();
		ArrayList<String> retrofitAdviceSection = new ArrayList<String>();
		for(int i=0;i<aspects.size();i++) {
			String srchAspect = aspects.get(i);
			ArrayList<String> inputAspect = fileio.input(modelicablockpath+srchAspect);
			ArrayList<String> resultAspect = interpret.fileCheck(inputAspect,aspectPattern);
			resultAspect = interpret.fileCheck(resultAspect,annotationPattern);
			
			convertAspect.addAll(interpret.interpretAnnotationAspectType(resultAspect,"ConvertAspect"));
			convertAspect = interpret.fileCheck(convertAspect, "advice");
			convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
			convertAdviceSection = interpret.fileCheck(convertAdviceSection,advicePattern);
			
			
			retrofitAspect.addAll( interpret.interpretAnnotationAspectType(resultAspect,"RetrofitAspect"));
			retrofitAspect = interpret.fileCheck(retrofitAspect, "advice");
			retrofitAdviceSection = interpret.interpretAdviceSection(retrofitAspect);
			retrofitAdviceSection = interpret.fileCheck(retrofitAdviceSection,advicePattern);
		}
		check(retrofitAdviceSection);
		assertThat(convertAdviceSection.get(0),is("  replace(signalVoltage)annotation(PointCut=\"DCmortorNONCAUSAL stepVoltage1\");"));
		assertThat(retrofitAdviceSection.get(0),is("  input(dcmotor.u)annotation(PointCut=\"DCmotorControll feedback1.u2\");"));
	}
	
	/**
	 * ごちゃごちゃ解消テスト2
	 * アドバイスを解釈し構文を生成する前までの段階
	 */
	@Test
	public void reviewTest2() {
	
		//１．ディレクトリ内に存在するアスペクトのファイル名を読み込む
		ArrayList<String> aspectFileNames = scp.findFile(modelicablockpath);
		//１．５.複数存在するであろうアスペクトファイルをひとつひとつ解釈する
		for(String aspectFileName : aspectFileNames) {
			//２．パスに存在するであろうアスペクトファイルを読み込む
			ArrayList<String> inputAspect = fileio.input(modelicablockpath+aspectFileName);
			//３．ファイルチェック　ルール通り記述されているか調べる。ここではaspect ○○という風にクラスが宣言されているか調べている
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			//４．ファイルチェック２　ここでは単独annotationが正しく記述されているか調べている
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			//５．C.単独アノテーションにおいてConvertAspectであるアスペクトをまとめる。
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			//5.R.単独アノテーションにおいてRetrofitAspectであるアスペクトをまとめる
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			if(convertAspect.size() != 0) {//変換アスペクトだった時の処理
				//６.C.ファイルチェック３　ここではadviceセクションが存在するか調べている
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				//7.C.アドバイスセクションを抜き出す
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				//8.C.アドバイスの構文のチェックを行う
				convertAdviceSection = interpret.syntaxCheck(convertAdviceSection,advicePattern);
				//9.5.C.ポイントカットの情報をScopeクラスに渡しモデルの探索を行う
				//**ArrayList<String> convertFindAspect = unknown(convertPointCut,convertAdvice)//ブロック線図モデルにおいて制御対象モデルを探す**
				//10.C.アドバイスに存在するアドバイス部分を抜き出す
				//ArrayList<String> convertAdvice = interpret.interpretAdvice();
				//check(convertAdvice);
			} else if(retrofitAspect.size() != 0) {//組み込みアスペクトだった時の処理
				//６.R.ファイルチェック３　ここではadviceセクションが存在するか調べている
				retrofitAspect = interpret.fileCheck(retrofitAspect, "advice");
				//7.C.アドバイスセクションを抜き出す
				ArrayList<String> retrofitAdviceSection = interpret.interpretAdviceSection(retrofitAspect);
				//8.C.アドバイスの構文のチェックを行う
				retrofitAdviceSection = interpret.syntaxCheck(retrofitAdviceSection,advicePattern);
				//9.5.R.ポイントカットの情報をScopeクラスに渡しモデルの探索を行う
				//**ArrayList<String> retrofitFindAspect = unknown(retrofitPointCut,retrofitAdvice)//ブロック線図モデルにおいて制御対象モデルを探す**
				//10.C.アドバイスに存在するアドバイス部分を抜き出す
				//ArrayList<String> retrofitAdvice = interpret.interpretAdvice();
				//check(retrofitAdvice);
			}
		}
	}
	
	/**
	 * アスペクトファイルに存在するインスタンス抜き出しテスト
	 */
	@Test
	public void divineInstanceSection() {
		ArrayList<String>convertInstanceSection = new ArrayList<String>();
		
		ArrayList<String> aspectFileNames = scp.findFile(modelicablockpath);
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicablockpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			if(convertAspect.size() != 0) {
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				convertInstanceSection = interpret.interpretInstanceSection(convertAspect);
			} else if(retrofitAspect.size() != 0) {
				retrofitAspect = interpret.fileCheck(retrofitAspect, "advice");
				ArrayList<String> retrofitAdviceSection = interpret.interpretAdviceSection(retrofitAspect);
				retrofitAdviceSection = interpret.syntaxCheck(retrofitAdviceSection,advicePattern);
			}
		}
		assertThat(convertInstanceSection.get(0),is("  Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage;"));
	}
	
	/**
	 * アドバイス構文を解釈しmodelica構文を生成するテスト
	 */
	@Test
	public void createModelicaSentence() {
		
		String modelicanowpath = "C:\\modelica\\Now\\";
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath+"DCmortorControll.mo");
		
		
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		
		ArrayList<String> aspectFileNames = scp.findFile(modelicablockpath);
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicablockpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			if(convertAspect.size() != 0) {
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				ArrayList<String> convertInstanceSection = interpret.interpretInstanceSection(convertAspect);
				convertAdviceSection = interpret.syntaxCheck(convertAdviceSection,advicePattern);
				
				//14.C.ファイル探索を行うための構文を作成
				ArrayList<String>convertPC = scp.createConvertSyntaxFromAdivce(convertAdviceSection,arrInBlock);
				ArrayList<String> arrPath = new ArrayList<String>();
				//15.C.構文一つ一つを評価していく
				for(String path :convertPC) {
					//16.C.構文を再帰的にファイルの探索を行い必要なものだけを入れる
					arrPath.addAll(scp.inputModel(path));
				}
				//17.C.ファイル探索構文をアドバイス構文に変換する
				ArrayList<String> searched = scp.createAdviceSyntax(arrPath);
				
				convertAdvice = interpret.interpretAdvice(searched,convertInstanceSection);
				check(convertAdvice);
			} else if(retrofitAspect.size() != 0) {
				retrofitAspect = interpret.fileCheck(retrofitAspect, "advice");
				ArrayList<String> retrofitAdviceSection = interpret.interpretAdviceSection(retrofitAspect);
				ArrayList<String> retrofitInstanceSection = interpret.interpretInstanceSection(retrofitAspect);
				retrofitAdvice = interpret.interpretAdvice(retrofitAdviceSection,retrofitInstanceSection);
				//check(retrofitAdvice);
			}
		}
		assertThat(convertAdvice.get(0),is("RI:  Modelica.Electrical.Analog.Sources.SignalVoltage stepVoltage1;"));
		assertThat(convertAdvice.size(),is(7));
		assertThat(retrofitAdvice.get(0),is("A:  connect(dcmotor.u,feedback1.u2);"));
		assertThat(retrofitAdvice.size(),is(2));
	}
}
