package test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import weaver.FileIO;

/**
 * @author watanabe
 *
 */
public class FileIOTest {
	
	private FileIO fileio;
	private String path = "C:\\Users\\watanabe\\git\\weaverprototype\\WeaverPrototype\\src\\weaver\\";
	private String modelicaaspectpath = "C:\\modelica\\Aspect\\";
	private String modelicatestpath = "C:\\modelica\\weavertest\\";
	
	private String oldFile = "test.txt";
	private String newFile2 = "newfile2.txt";
	private String newFile3 = "newFile3.txt";
	private String newFile4 = "newFile4.txt";
	private String newFile5 = "newFile5.txt";
	private String DCmortorModel = "DCmortorNONCAUSAL.mo";
	private String OutModelicaFile = "CreateModelica.mo";
	private String outModelicaFile3 = "CreateModelica3.mo";
	private String outModelicaFile4 = "CreateModelica4.mo";
	private String outModelicaFile5 = "CreateModelica5.mo";
	private String outModelicaFile6 = "DCmortorCAUSAL.mo";
	
	private String angleSensor = "  Modelica.Mechanics.Rotational.Sensors.AngleSensor angleSensor1 annotation(Placement(visible = true, transformation(origin = {78, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));";
	private String connectSensorMotor = "  connect(mortor.flange_b, angleSensor1.flange) annotation(Line(points = {{60, 0}, {70, 0}, {70, -34}, {68, -34}, {68, 0}, {68, 0}}));";
	private String realInput = "  Modelica.Blocks.Interfaces.RealInput u annotation(Placement(visible = true, transformation(origin = {82, 40}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {82, 40}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));";
	private String realOutput = "  Modelica.Blocks.Interfaces.RealOutput y annotation(Placement(visible = true, transformation(origin = {-78, 48}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-78, 48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));";
	private String connectSensorOut = "  connect(angleSensor1.phi, u) annotation(Line(points = {{86, 8}, {96, 8}, {96, 52}, {92, 52}}, color = {0, 0, 127}));";
	private String connectActuatorIn = "  connect(y, stepVoltage1.v) annotation(Line(points = {{-98, 54}, {-76, 54}, {-76, 0}, {-74, 0}}, color = {0, 0, 127}));";
	private String connectPIDDcmortor = "  connect(PID.y, dcmotor.y) annotation(Line(points = {{34, 16}, {48, 16}, {48, 26}, {48, 26}}, color = {0, 0, 127}));";
	private String connectFeedbackDcmortor = "  connect(dcmotor.u, feedback1.u2) annotation(Line(points = {{66, 24}, {84, 24}, {84, -52}, {-36, -52}, {-36, 10}, {-36, 10}}, color = {0, 0, 127}));";
	
	@Before
	public void setUp() throws Exception {
		this.fileio = new FileIO();
	}

	/**
	 * ファイルを読み込むテスト
	 */
	@Test
	public void inputTest() {
		ArrayList<String> arrResult = fileio.input(path + oldFile);
		assertThat(arrResult.get(arrResult.size()-1),is("あ"));
	}
	
	/**
	 * モデリカファイルを読み込むテストs
	 */
	@Test
	public void dcmortorNONCAUSALinputTest() {
		ArrayList<String> arrModelica = fileio.input(modelicaaspectpath+DCmortorModel);
		String first = arrModelica.get(0);
		String last  = arrModelica.get(arrModelica.size()-1);
		assertThat(first,is("model DCmortorNONCAUSAL"));
		assertThat(last,is("end DCmortorNONCAUSAL;"));
		
	}
	
	/**
	 * ファイルを読み込み新しいファイルに出力するテスト
	 */
	@Test
	public void outputTest() {
		int cnt = 0;
		
		ArrayList<String> arrIn = fileio.input(path+oldFile);
		ArrayList<String> arrOut = fileio.output(arrIn,(path+newFile2));
		//ArrayList<String> arrRes = fileio.input(path+newFile2);
		while(arrOut.get(cnt).equals(arrIn.get(cnt)) && cnt < arrIn.size()-1) {
			cnt++;
		}
		assertThat(cnt,is(arrIn.size()-1));
	}
	

	
	/**
	 * ファイルを読み込み別の新しいファイルに出力するテスト
	 */
	@Test
	public void inputArrayListOutputToNewFile() {
		ArrayList<String> arrFile = fileio.input(path+oldFile);
		ArrayList<String> arrOut = fileio.output(arrFile,newFile2);
		assertThat(arrOut.get(0),is(arrFile.get(0)));
	}
	
	/**
	 * ファイルを読み込み指定したインデックス（行）に対して文字列を追加するテストs
	 */
	@Test
	public void outputWriteString() {
		ArrayList<String> arrIn = fileio.input(path+oldFile);
		arrIn = fileio.writeString(arrIn,"model test",0);
		arrIn = fileio.writeString(arrIn,"equation",3);
		arrIn = fileio.writeString(arrIn, "end test",arrIn.size());
		assertThat(arrIn.get(0),is("model test"));
		assertThat(arrIn.get(3),is("equation"));
		assertThat(arrIn.get(arrIn.size()-1),is("end test"));
//		for(int i = 0;i<arrIn.size();i++) {
//			System.out.println(arrIn.get(i));
//		}
	}
	
	/**
	 * ファイルを読み込み指定したインデックス（行）に対して文字列を追加しファイルに出力するテスト
	 */
	@Test
	public void inFileWriteStringOutNewFile3() {
		ArrayList<String> arrIn = fileio.input(path+oldFile);
		arrIn = fileio.writeString(arrIn,"model test",0);
		arrIn = fileio.writeString(arrIn,"equation",3);
		arrIn = fileio.writeString(arrIn, "end test",arrIn.size());
		ArrayList<String> arrOut = fileio.output(arrIn,(path+newFile3));
		assertThat(arrOut.get(0),is("model test"));
		assertThat(arrOut.get(3),is("equation"));
		assertThat(arrOut.get(arrOut.size()-1),is("end test"));
	}
	
	/**
	 * ファイルを読み込み指定したインデックス（行）に対してモデリカ構文を追加しファイルに出力するテスト
	 */
	@Test
	public void inFileWriteModelicaStringOUtNewFile4() {
		ArrayList<String> arrIn = fileio.input(path+newFile3);
		arrIn = fileio.writeString(arrIn, "Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage;", 1);
		arrIn = fileio.writeString(arrIn, "connect(stepVoltage1.p, ground1.p);", 5);
		arrIn = fileio.writeString(arrIn, "connect(stepVoltage1.n, R.p);", 5);
		ArrayList<String> arrOut = fileio.output(arrIn, (path+newFile4));
		assertThat(arrOut.get(1),is("Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage;"));
		assertThat(arrOut.get(5),is("connect(stepVoltage1.n, R.p);"));
		assertThat(arrOut.get(6),is("connect(stepVoltage1.p, ground1.p);"));
	}
	
	/**
	 * ファイルを読み込みある文字列を探索するテスト
	 */
	@Test
	public void inFileSearchModelicaString() {
		ArrayList<String> arrIn = fileio.input(path+newFile4);
		ArrayList<Integer> indxs = fileio.searchKeyword(arrIn,"model test");
		assertThat(arrIn.get(indxs.get(0)),is("model test"));
		ArrayList<Integer> indxs2 = fileio.searchKeyword(arrIn, "end test");
		assertThat(arrIn.get(indxs2.get(0)),is("end test"));
		ArrayList<Integer> indxs3 = fileio.searchKeyword(arrIn, "unko");
		assertThat(indxs3.get(0),is(-1));
	}
	
	/**
	 * ファイルを読み込みある文字列を探索する。見つかればその下の行に文字列を追加しファイルに出力するテストs
	 */
	@Test
	public void inFileSearchStringWriteStringOutFile5() {
		ArrayList<String> arrIn = fileio.input(path+newFile4);
		ArrayList<Integer> indxs = fileio.searchKeyword(arrIn, "model test");
		arrIn = fileio.writeString(arrIn, "Modelica.Mechanics.Rotational.Sensors.AngleSensor angleSensor;", indxs.get(0)+1);
		ArrayList<String> arrOut = fileio.output(arrIn, (path+newFile5));
		
		ArrayList<Integer> results = fileio.searchKeyword(arrOut, "Modelica.Mechanics.Rotational.Sensors.AngleSensor angleSensor;");
		assertThat(arrOut.get(results.get(0)),is("Modelica.Mechanics.Rotational.Sensors.AngleSensor angleSensor;"));
		
		ArrayList<Integer> indxs2 = fileio.searchKeyword(arrOut, "equation");
		arrOut = fileio.writeString(arrIn, "connect(feedback1.y, PID.u);", indxs2.get(0)+1);
		ArrayList<String> arrAddConnect = fileio.output(arrOut, (path+newFile5));
		
		ArrayList<Integer> results2 = fileio.searchKeyword(arrAddConnect, "connect(feedback1.y, PID.u);");
		assertThat(arrAddConnect.get(results2.get(0)),is("connect(feedback1.y, PID.u);"));
	}
	
	/**
	 * モデリカファイルを読み込みモデリカファイルを別ファイルとして書き出すテスト
	 */
	@Test
	public void inModelicaFileOutModelicaFile() {
		ArrayList<String> arrIn = fileio.input(modelicaaspectpath+DCmortorModel);
		ArrayList<String> arrOut = fileio.output(arrIn,(path+OutModelicaFile));
		assertThat(arrOut.get(0),is("model DCmortorNONCAUSAL"));
	}
	
	/**
	 * モデリカファイルを読み込みあるキーワードが含まれているか調べるテスト
	 */
	@Test
	public void inModelicaFileContainsKeyword() {
		ArrayList<String> arrIn = fileio.input(modelicaaspectpath+DCmortorModel);
		ArrayList<Integer> indxs = fileio.searchKeyword(arrIn, "DCmortorNONCAUSAL");
		assertThat(indxs.get(0),is(0));
		assertThat(indxs.get(1),is(35));
	}
	
	
	/**
	 * モデリカファイルを読み込み特定の文字列をあるキーワードに変更するテスト
	 */
	@Test
	public void inModelicaFileReplaceSpecificKeyword() {
		ArrayList<String> arrIn = fileio.input(modelicaaspectpath+DCmortorModel);
		ArrayList<Integer> indxs = fileio.searchKeyword(arrIn, "DCmortorNONCAUSAL");
		ArrayList<String> results = fileio.replaceKeyword(arrIn,indxs,"CreateModelica","DCmortorNONCAUSAL");
		assertThat(results.get(0),is("model CreateModelica"));
		assertThat(results.get(35),is("end CreateModelica;"));
	}
	
	/**
	 * モデリカファイルを読み込みファイル名をCreateModelicaとし、中身のモデル名もCreateModelicaに変更して書き出すテスト
	 */
	@Test
	public void inModelicaFileChangeFileNameOutModelicaFile() {
		ArrayList<String> arrIn = fileio.input(modelicaaspectpath+DCmortorModel);
		ArrayList<Integer> indxs = fileio.searchKeyword(arrIn, "DCmortorNONCAUSAL");
		ArrayList<String> results = fileio.replaceKeyword(arrIn,indxs,"CreateModelica","DCmortorNONCAUSAL");
		ArrayList<String> arrOut = fileio.output(results, (modelicatestpath+OutModelicaFile));
		assertThat(arrOut.get(0),is("model CreateModelica"));
	}
	
	/**
	 * 非因果制御対象モデルを読み込みセンサインスタンス生成文とセンサとモーターを繋ぐconnect文を加えたうえで別ファイルにかきだす
	 */
	@Test
	public void inModelicaFileAddInstanceCreateAndConnectOutModelicaFile() {
		ArrayList<String> arrIn = fileio.input(modelicaaspectpath+DCmortorModel);
		ArrayList<Integer> indxs = fileio.searchKeyword(arrIn, "model DCmortorNONCAUSAL");
		arrIn = fileio.writeString(arrIn, angleSensor, indxs.get(0)+1);
		indxs = fileio.searchKeyword(arrIn,"equation");
		arrIn = fileio.writeString(arrIn,connectSensorMotor,indxs.get(0)+1);
		ArrayList<String> arrOut = fileio.output(arrIn, modelicatestpath+outModelicaFile3);
		assertNotNull(arrOut);
		
		ArrayList<Integer> indxs2 = fileio.searchKeyword(arrIn, "DCmortorNONCAUSAL");
		ArrayList<String> results = fileio.replaceKeyword(arrIn,indxs2,"CreateModelica3","DCmortorNONCAUSAL");
		ArrayList<String> arrOut2 = fileio.output(results, (modelicatestpath+outModelicaFile3));
		assertThat(arrOut2.get(0),is("model CreateModelica3"));
		assertThat(arrOut2.get(1),is(angleSensor));
	}
	
	
	/**
	 * 非因果制御対象モデルを読み込みアクチュエータの置換を行ったうえで別ファイルに書き出す
	 */
	@Test
	public void inModelicaFileReplaceActuatorOutModelicaDile() {
		ArrayList<String> arrIn = fileio.input(modelicaaspectpath+DCmortorModel);
		ArrayList<Integer> indxs = fileio.searchKeyword(arrIn, "StepVoltage");
		arrIn = fileio.replaceKeyword(arrIn,indxs,"SignalVoltage","StepVoltage");
		arrIn = fileio.output(arrIn, modelicatestpath+outModelicaFile4);
		
		indxs = fileio.searchKeyword(arrIn, "SignalVoltage");
		assertThat(arrIn.get(indxs.get(0)),is("  Modelica.Electrical.Analog.Sources.SignalVoltage stepVoltage1 annotation("));
		
		indxs = fileio.searchKeyword(arrIn, "DCmortorNONCAUSAL");
		arrIn = fileio.replaceKeyword(arrIn, indxs, "CreateModelica4", "DCmortorNONCAUSAL");
		ArrayList<String> arrOut = fileio.output(arrIn, modelicatestpath+outModelicaFile4);
		
		assertThat(arrOut.get(0),is("model CreateModelica4"));
	}
	
	/**
	 * 非因果制御対象モデルを読み込み因果的なコネクタのインスタンスの生成を行い別のファイルに書き出す
	 */
	@Test
	public void inModelicaFileAddInstanceCreateOutModelicaFile() {
		ArrayList<String> arrIn = fileio.input(modelicaaspectpath+DCmortorModel);
		ArrayList<Integer> indxs = fileio.searchKeyword(arrIn, "model DCmortorNONCAUSAL");
		arrIn = fileio.writeString(arrIn,  realInput, indxs.get(0)+1);
		arrIn = fileio.writeString(arrIn, realOutput, indxs.get(0)+1);
		ArrayList<String> arrOut = fileio.output(arrIn, modelicatestpath+outModelicaFile5);
		
		assertThat(arrOut.get(1),is(realOutput));
		assertThat(arrOut.get(2),is(realInput));
		
		indxs = fileio.searchKeyword(arrIn, "DCmortorNONCAUSAL");
		arrIn = fileio.replaceKeyword(arrIn, indxs, "CreateModelica5", "DCmortorNONCAUSAL");
		arrOut = fileio.output(arrIn, modelicatestpath+outModelicaFile5);
		
		assertThat(arrOut.get(0),is("model CreateModelica5"));
	}
	
	/**
	 * 非因果制御対象モデルを読み込み因果的なモデルに変換し別のファイルに書き出す
	 */
	@Test
	public void inModelicaFileCompleteCausalModelOutModelicaFile() {
		//１．変換対象の非因果制御対象を読み込む
		ArrayList<String> arrIn = fileio.input(modelicaaspectpath+DCmortorModel);
		//２．インスタンスの生成
		ArrayList<Integer> indxs = fileio.searchKeyword(arrIn, "model DCmortorNONCAUSAL");
		arrIn = fileio.writeString(arrIn, angleSensor, indxs.get(0)+1);
		arrIn = fileio.writeString(arrIn, realInput, indxs.get(0)+1);
		arrIn = fileio.writeString(arrIn, realOutput, indxs.get(0)+1);
		//３．アクチュエータへの置換
		indxs = fileio.searchKeyword(arrIn, "StepVoltage");
		arrIn = fileio.replaceKeyword(arrIn,indxs,"SignalVoltage","StepVoltage");
		//４．connect文の生成
		indxs = fileio.searchKeyword(arrIn, "equation");
		arrIn = fileio.writeString(arrIn, connectSensorMotor, indxs.get(0)+1);
		arrIn = fileio.writeString(arrIn, connectSensorOut, indxs.get(0)+1);
		arrIn = fileio.writeString(arrIn, connectActuatorIn, indxs.get(0)+1);
		//５．モデル名をファイル名に変更する
		indxs = fileio.searchKeyword(arrIn, "DCmortorNONCAUSAL");
		arrIn = fileio.replaceKeyword(arrIn, indxs, "DCmortorCAUSAL", "DCmortorNONCAUSAL");
		//６．ファイルを出力
		ArrayList<String> arrOut = fileio.output(arrIn, modelicatestpath+outModelicaFile6);
		
		assertThat(arrOut.get(0),is("model DCmortorCAUSAL"));
		assertThat(arrOut.get(arrOut.size()-1),is("end DCmortorCAUSAL;"));
	}
	

	/**
	 * 非因果制御対象モデルとブロック線図モデルを読み込み完成されたブロック線図モデルを出力する。
	 */
	@Test
	public void inModelicaFilesCompleteOutModelicaFile() {
		//１．変換対象の非因果制御対象を読み込む
		ArrayList<String> arrIn = fileio.input(modelicaaspectpath+DCmortorModel);
		//２．インスタンスの生成
		ArrayList<Integer> indxs = fileio.searchKeyword(arrIn, "model DCmortorNONCAUSAL");
		arrIn = fileio.writeString(arrIn, angleSensor, indxs.get(0)+1);
		arrIn = fileio.writeString(arrIn, realInput, indxs.get(0)+1);
		arrIn = fileio.writeString(arrIn, realOutput, indxs.get(0)+1);
		//３．アクチュエータへの置換
		indxs = fileio.searchKeyword(arrIn, "StepVoltage");
		arrIn = fileio.replaceKeyword(arrIn,indxs,"SignalVoltage","StepVoltage");
		//４．connect文の生成
		indxs = fileio.searchKeyword(arrIn, "equation");
		arrIn = fileio.writeString(arrIn, connectSensorMotor, indxs.get(0)+1);
		arrIn = fileio.writeString(arrIn, connectSensorOut, indxs.get(0)+1);
		arrIn = fileio.writeString(arrIn, connectActuatorIn, indxs.get(0)+1);
		//５．モデル名をファイル名に変更する
		indxs = fileio.searchKeyword(arrIn, "DCmortorNONCAUSAL");
		arrIn = fileio.replaceKeyword(arrIn, indxs, "DCmortorCAUSAL", "DCmortorNONCAUSAL");
		//６．ファイルを出力
		ArrayList<String> arrOut = fileio.output(arrIn, modelicatestpath+outModelicaFile6);
		assertNotNull(arrOut);
		
		//１.完成させるべきブロック線図モデルを読み込む
		ArrayList<String> arrInBlock = fileio.input(modelicaaspectpath+"DCmortorControll.mo");
		//２.非因果制御対象モデルの宣言を変換した因果制御対象モデルに変更する
		ArrayList<Integer> indxsBlock = fileio.searchKeyword(arrInBlock, "DCmortorNONCAUSAL");
		arrInBlock = fileio.replaceKeyword(arrInBlock, indxsBlock, "DCmortorCAUSAL", "DCmortorNONCAUSAL");
		//３．制御対象モデルを接続するためのconnect文の生成
		indxsBlock = fileio.searchKeyword(arrInBlock, "equation");
		arrInBlock = fileio.writeString(arrInBlock, connectPIDDcmortor, indxsBlock.get(0)+1);
		arrInBlock = fileio.writeString(arrInBlock, connectFeedbackDcmortor, indxsBlock.get(0)+1);
		//４.モデル名をファイル名に変更する
		indxs = fileio.searchKeyword(arrInBlock, "DCmortorControll");
		arrIn = fileio.replaceKeyword(arrInBlock, indxs, "DCmortorControllComplete", "DCmortorControll");
		//５.ファイルを出力
		ArrayList<String> arrOutBlock = fileio.output(arrInBlock, modelicatestpath+"DCmortorControllComplete.mo");
		
		assertThat(arrOutBlock.get(0),is("model DCmortorControllComplete"));
	}
}
