package test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import weaver.FileIO;
import weaver.Interpreter;
import weaver.Scope;

public class ConveyorTest {

	private FileIO fileio;
	private Scope  scp;
	private Interpreter interpret;
	
	private String modelicanowpath = "C:\\modelica\\Now\\";
	
	private String aspectPattern ="aspect\\s[a-zA-Z0-9]*";
	private String instancePattern ="\\s*[[a-zA-Z0-9_]*.]*[a-zA-Z0-9_]+\\s[a-zA-Z0-9_]+;";
	private String annotationPattern = "annotation\\(Aspect=\"[a-zA-Z]*\"\\);";
	private String pointcutPattern = "[[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\s[[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_]+";
	private String advicePattern ="\\s*[a-z]*\\([[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\)annotation\\(PointCut=\""+pointcutPattern +"\"\\);";

	private String nonCausalModel="";
	String model = "DCmortorControll";
	
	public void check(ArrayList<String> arrList) {
		for(int i=0;i<arrList.size();i++) {
			System.out.println(arrList.get(i));
		}
	}


	@Before
	public void setUp() throws Exception {
		this.scp = new Scope();
		this.fileio = new FileIO();
		this.interpret = new Interpreter();
	}

	/**
	 * ブロック線図を読み込むテスト
	 */
	@Test
	public void inputBlockDiagramTest() {
		
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");
		assertTrue(arrInBlock.get(0).matches("\\s*model\\s"+model));
	}
	
	/**
	 * アスペクトファイルを見つけるテスト
	 */
	@Test
	public void findAllAspectAndInputBlockDiagramFile() {
		
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");
		
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		assertTrue(arrInBlock.get(0).matches("\\s*model\\s"+model));
		assertNotNull(aspectFileNames);
		
	}
	
	/**
	 * アスペクトファイルを読み込むテスト
	 */
	@Test
	public void inputAspectFile() {
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");
		
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			assertNotNull(inputAspect.get(0));
		}
		assertTrue(arrInBlock.get(0).matches("\\s*model\\s"+model));
		assertNotNull(convertAdvice);
		assertNotNull(retrofitAdvice);
	}
	
	/**
	 * アスペクトファイルにおいてルール通りの記述が行われているかチェックを行う
	 */
	@Test
	public void aspectCheck() {
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");
		
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			if(rightAspect.size()!=0) {
				if(!rightAspect.get(0).contains("//"))assertTrue(rightAspect.get(0).matches(aspectPattern));
				else {
					assertTrue(rightAspect.get(1).split("//")[0].matches(aspectPattern));
				}
			}
		}
		assertTrue(arrInBlock.get(0).matches("\\s*model\\s"+model));
		assertNotNull(convertAdvice);
		assertNotNull(retrofitAdvice);
	}
	
	/**
	 * アスペクトファイルにおいて単独アノテーションがルール通りの記述が行われているかチェックを行う
	 */
	@Test
	public void annotationCheck() {
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");	
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			if(rightAspect.size()!=0) {
				if(!rightAspect.get(0).contains("//"))assertTrue(rightAspect.get(0).matches(aspectPattern));
				else {
					assertTrue(rightAspect.get(1).split("//")[0].matches(aspectPattern));
				}
			}
		}
		assertTrue(arrInBlock.get(0).matches("\\s*model\\s"+model));
		assertNotNull(convertAdvice);
		assertNotNull(retrofitAdvice);
	}
	
	/**
	 * アスペクトの種類を区別するテスト(convertAspect)
	 */
	@Test
	public void convertAspectTest() {
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");	
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			for(String testl : convertAspect) {
				if(testl.contains("ConvertAspect")) {
					assertTrue(true);
				}
			}
		}
		assertTrue(arrInBlock.get(0).matches("\\s*model\\s"+model));
		assertNotNull(convertAdvice);
		assertNotNull(retrofitAdvice);
	}
	
	/**
	 * アスペクトの種類を区別するテスト(retrofitAspect)
	 */
	@Test
	public void retrofitAspectTest() {
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");	
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			for(String testl : convertAspect) {
				if(testl.contains("ConvertAspect")) {
					assertTrue(true);
				}
			}
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			for(String testl : retrofitAspect) {
				if(testl.contains("RetrofitAspect")) {
					assertTrue(true);
				}
			}
		}
		assertTrue(arrInBlock.get(0).matches("\\s*model\\s"+model));
		assertNotNull(convertAdvice);
		assertNotNull(retrofitAdvice);
	}
	
	/**
	 * ConvertAspectにおいてadviceセクションが存在するか確かめるテスト
	 */
	@Test
	public void convertCheckAdvice() {
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");	
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			for(String testl : retrofitAspect) {
				if(testl.contains("RetrofitAspect")) {
					assertTrue(true);
				}
			}
			if(convertAspect.size() != 0) {
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				for(String testl : convertAspect) {
					testl = testl.split("//")[0];
					if(testl.matches("\\s*advice")) {
						assertTrue(true);
					}
				}
			}
		}
		assertTrue(arrInBlock.get(0).matches("\\s*model\\s"+model));
		assertNotNull(convertAdvice);
		assertNotNull(retrofitAdvice);
	}
	
	/**
	 * ConvertAspectにてアドバイスセクションを抜き出すテスト
	 */
	@Test
	public void convertTakeAdviceSection() {
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");	
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			for(String testl : retrofitAspect) {
				if(testl.contains("RetrofitAspect")) {
					assertTrue(true);
				}
			}
			if(convertAspect.size() != 0) {
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				for(String testl : convertAdviceSection ) {
					assertTrue(testl.matches(advicePattern));
				}
			}
		}
		assertTrue(arrInBlock.get(0).matches("\\s*model\\s"+model));
		assertNotNull(convertAdvice);
		assertNotNull(retrofitAdvice);
	}
	
	/**
	 * ConvertAspectにおいてインスタンスセクションを抜き出すテスト
	 */
	@Test
	public void convertTakeInstanceSection() {
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");	
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			for(String testl : retrofitAspect) {
				if(testl.contains("RetrofitAspect")) {
					assertTrue(true);
				}
			}
			if(convertAspect.size() != 0) {
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				for(String testl : convertAdviceSection ) {
					assertTrue(testl.matches(advicePattern));
				}
				ArrayList<String> convertInstanceSection = interpret.interpretInstanceSection(convertAspect);
				for(String testl : convertInstanceSection) {
					assertTrue(testl.matches(instancePattern));
				}
			}
		}
		assertTrue(arrInBlock.get(0).matches("\\s*model\\s"+model));
		assertNotNull(convertAdvice);
		assertNotNull(retrofitAdvice);
	}
	
	@Test
	public void convertSyntaxCheckAdvice() {
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");	
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			for(String testl : retrofitAspect) {
				if(testl.contains("RetrofitAspect")) {
					assertTrue(true);
				}
			}
			if(convertAspect.size() != 0) {
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				ArrayList<String> convertInstanceSection = interpret.interpretInstanceSection(convertAspect);
				for(String testl : convertInstanceSection) {
					assertTrue(testl.matches(instancePattern));
				}
				convertAdviceSection = interpret.syntaxCheck(convertAdviceSection,advicePattern);
				//check(convertAdviceSection);
				for(String testl: convertAdviceSection) {
					assertTrue(testl.matches(advicePattern));
				}
			}
		}
		assertTrue(arrInBlock.get(0).matches("\\s*model\\s"+model));
		assertNotNull(convertAdvice);
		assertNotNull(retrofitAdvice);
	}
	
	/**
	 * convertAspectにおいてアドバイス構文からファイル探索構文を生成するテスト
	 */
	@Test
	public void convertSearchSyntaxFromAdvice() {
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");	
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			for(String testl : retrofitAspect) {
				if(testl.contains("RetrofitAspect")) {
					assertTrue(true);
				}
			}
			if(convertAspect.size() != 0) {
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				ArrayList<String> convertInstanceSection = interpret.interpretInstanceSection(convertAspect);
				for(String testl : convertInstanceSection) {
					assertTrue(testl.matches(instancePattern));
				}
				convertAdviceSection = interpret.syntaxCheck(convertAdviceSection,advicePattern);
				ArrayList<String>convertPC = scp.createConvertSyntaxFromAdivce(convertAdviceSection,arrInBlock);
				check(convertPC);
				for(String testl : convertPC) {
					assertTrue(testl.split(":")[0].matches("[[a-zA-Z0-9_]*.]*[a-zA-Z0-9_]+"));
					assertTrue(testl.split(":")[1].matches("[[a-zA-Z0-9_]*.]*[a-zA-Z0-9_]+"));
					assertTrue(testl.split(":")[2].matches(advicePattern));
					String testl2 = testl.split("\"")[1].split("\\s")[1];
					assertThat(testl.split(":")[1],is(testl2));
				}
			}
		}
		assertNotNull(convertAdvice);
		assertNotNull(retrofitAdvice);
	}
	
	/**
	 * convertAspectにおいてファイル探索構文から新たなファイル探索構文を作成し最終的に使えるアドバイスだけが残るか確認するテスト
	 */
	@Test
	public void convertSearchSyntaxFromSybtax () {
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");	
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			for(String testl : retrofitAspect) {
				if(testl.contains("RetrofitAspect")) {
					assertTrue(true);
				}
			}
			if(convertAspect.size() != 0) {
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				ArrayList<String> convertInstanceSection = interpret.interpretInstanceSection(convertAspect);
				for(String testl : convertInstanceSection) {
					assertTrue(testl.matches(instancePattern));
				}
				convertAdviceSection = interpret.syntaxCheck(convertAdviceSection,advicePattern);
				ArrayList<String>convertSrch = scp.createConvertSyntaxFromAdivce(convertAdviceSection,arrInBlock);
				ArrayList<String> convertSrchSyntax = new ArrayList<String>();
				for(String path :convertSrch) {
					convertSrchSyntax.addAll(scp.inputModel(path));
				}
				for(String testl : convertSrchSyntax) {
					assertTrue(testl.contains("><"));
				}
				//check(convertSrchSyntax);
			}
		}
		assertNotNull(convertAdvice);
		assertNotNull(retrofitAdvice);
	}
	
	/**
	 * convertAspectにおいて探索が成功したアドバイスにて非因果制御対象モデルの名前を取得するテスト
	 */
	@Test
	public void takeNonCausalModelNameTest() {

		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");	
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			for(String testl : retrofitAspect) {
				if(testl.contains("RetrofitAspect")) {
					assertTrue(true);
				}
			}
			if(convertAspect.size() != 0) {
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				ArrayList<String> convertInstanceSection = interpret.interpretInstanceSection(convertAspect);
				for(String testl : convertInstanceSection) {
					assertTrue(testl.matches(instancePattern));
				}
				convertAdviceSection = interpret.syntaxCheck(convertAdviceSection,advicePattern);
				ArrayList<String>convertSrch = scp.createConvertSyntaxFromAdivce(convertAdviceSection,arrInBlock);
				ArrayList<String> convertSrchSyntax = new ArrayList<String>();
				for(String path :convertSrch) {
					convertSrchSyntax.addAll(scp.inputModel(path));
				}
				nonCausalModel = interpret.takeNonCausalModelName(convertSrchSyntax);
				assertTrue(nonCausalModel.matches("[a-zA-Z0-9_]*"));
				//System.out.println(nonCausalModel);
			}
		}
		assertNotNull(convertAdvice);
		assertNotNull(retrofitAdvice);
	}
	
	/**
	 * convertAspectにおいて探索した結果からアドバイス構文を生成するテスト
	 */
	@Test
	public void convertCreateAdviceSyntax() {
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");	
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			for(String testl : retrofitAspect) {
				if(testl.contains("RetrofitAspect")) {
					assertTrue(true);
				}
			}
			if(convertAspect.size() != 0) {
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				ArrayList<String> convertInstanceSection = interpret.interpretInstanceSection(convertAspect);
				for(String testl : convertInstanceSection) {
					assertTrue(testl.matches(instancePattern));
				}
				convertAdviceSection = interpret.syntaxCheck(convertAdviceSection,advicePattern);
				ArrayList<String>convertSrch = scp.createConvertSyntaxFromAdivce(convertAdviceSection,arrInBlock);
				ArrayList<String> convertSrchSyntax = new ArrayList<String>();
				for(String path :convertSrch) {
					convertSrchSyntax.addAll(scp.inputModel(path));
				}
				nonCausalModel = interpret.takeNonCausalModelName(convertSrchSyntax);
				ArrayList<String> searched = scp.createAdviceSyntax(convertSrchSyntax);
				//check(searched);
				for(String testl : searched) {
					assertTrue(testl.matches(advicePattern));
				}
			}
		}
		assertNotNull(convertAdvice);
		assertNotNull(retrofitAdvice);
	}
	
	/**
	 * convertApsectにおいてアドバイス構文からModelica構文を生成するテスト
	 */
	@Test
	public void convertCreateModelicaSyntax() {
		
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath + model + ".mo");	
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			for(String testl : retrofitAspect) {
				if(testl.contains("RetrofitAspect")) {
					assertTrue(true);
				}
			}
			if(convertAspect.size() != 0) {
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				ArrayList<String> convertInstanceSection = interpret.interpretInstanceSection(convertAspect);

				convertAdviceSection = interpret.syntaxCheck(convertAdviceSection,advicePattern);
				ArrayList<String>convertSrch = scp.createConvertSyntaxFromAdivce(convertAdviceSection,arrInBlock);
				ArrayList<String> convertSrchSyntax = new ArrayList<String>();
				for(String path :convertSrch) {
					convertSrchSyntax.addAll(scp.inputModel(path));
				}
				nonCausalModel = interpret.takeNonCausalModelName(convertSrchSyntax);
				ArrayList<String> searched = scp.createAdviceSyntax(convertSrchSyntax);
				convertAdvice = interpret.interpretAdvice(searched,convertInstanceSection);
				//check(convertAdvice);
			}
		}
		assertNotNull(convertAdvice);
		assertNotNull(retrofitAdvice);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * ポイントカットにアスタリスクがあった場合の不具合解消
	 * PointCut="stepVoltage1"のように.で区切られていないポイントカットが存在しモデルにextendsを含む場合、探索構文を作成するときに不具合が生じていた
	 */
	@Test
	public void replaceAstinputModelTest() {
		ArrayList<String>convertSrch =new ArrayList<String>();
		convertSrch.add("Modelica.Blocks.Sources.Step:stepVoltage1:  replace(signalVoltage)annotation(PointCut=\"* stepVoltage1\");11");
		convertSrch.add("Modelica.Blocks.Continuous.PID:stepVoltage1:  replace(signalVoltage)annotation(PointCut=\"* stepVoltage1\");22");
		convertSrch.add("DCmortorNONCAUSAL:stepVoltage1:  replace(signalVoltage)annotation(PointCut=\"* stepVoltage1\");33");
		convertSrch.add("DCmortorControll:PID.y:  output(dcmotor.y)annotation(PointCut=\"DCmortorControll PID.y\");");
		ArrayList<String> convertSrchSyntax = new ArrayList<String>();
		for(String path :convertSrch) {
			convertSrchSyntax.addAll(scp.inputModel(path));
		}
		//check(convertSrchSyntax);
	}

}
