/**
 * 
 */
package test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import weaver.FileIO;
import weaver.Interpreter;
import weaver.Scope;

/**
 * @author watanabe
 *
 */
public class ScopeTest {
	
	private FileIO fileio;
	private Scope  scp;
	private Interpreter interpret;
	
	private String modelicablockpath = "C:\\modelica\\blockdiagram\\";
	private String modelicaaspectpath = "C:\\modelica\\Aspect\\";
	private String modelicanowpath = "C:\\modelica\\Now\\";
	
	private String aspectPattern ="aspect\\s[a-zA-Z0-9]*";
	private String annotationPattern = "annotation\\(Aspect=\"[a-zA-Z]*\"\\);";
	private String pointcutPattern = "[[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\s[[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_]+";
	private String advicePattern ="\\s*[a-z]*\\([[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\)annotation\\(PointCut=\""+pointcutPattern +"\"\\);";

	
	private String DCmortorModel = "DCmortorNONCAUSAL.mo";
	
	public void check(ArrayList<String> arrList) {
		for(int i=0;i<arrList.size();i++) {
			System.out.println(arrList.get(i));
		}
	}


	@Before
	public void setUp() throws Exception {
		this.scp = new Scope();
		this.fileio = new FileIO();
		this.interpret = new Interpreter();
	}

	/**
	 * 特定のディレクトリに存在するファイルを調べその中からAspectを含むファイルを探すテストs
	 */
	@Test
	public void findDirIntoFileTest() {
		ArrayList<String> aspects = scp.findFile(modelicablockpath);
		for(int i=0;i<aspects.size();i++) {
			String srchAspect = aspects.get(i);
			if(srchAspect.contains("BlockDiagramAspect2")) {
				assertThat(srchAspect,is("BlockDiagramAspect2.mo"));
			}
			if(srchAspect.contains("DCmotorAspect5.mo")) {
				assertThat(srchAspect,is("DCmotorAspect5.mo"));
			}
		}
	}
	
	/**
	 * パスで指定したディレクトリに存在する特定の複数のアスペクトファイルを読み込むテスト
	 */
	@Test
	public void inputAllAspectsInDirTest() {
		ArrayList<String> aspects = scp.findFile(modelicablockpath);
		for(int i=0;i<aspects.size();i++) {
			String srchAspect = aspects.get(i);
			if(srchAspect.contains("BlockDiagramAspect3.mo")) {
				ArrayList<String> aspect = fileio.input(modelicablockpath+srchAspect);
				String[] result = aspect.get(1).split("//",0);
				assertThat(result[0], is("aspect BlockDiagramAspect3"));
			} 
			if(srchAspect.contains("DCmotorAspect5.mo")) {
				ArrayList<String> aspect = fileio.input(modelicablockpath+srchAspect);
				String[] result = aspect.get(0).split("//",0);
				assertThat(result[0], is("aspect DCmotorAspect5"));
			}
			
		}
	}
	
	/**
	 * ブロック線図モデルのモデリカファイルを読み込んだ上、パスで指定したディレクトリに存在する特定の複数のアスペクトファイルを読み込むテスト
	 */
	@Test
	public void inputBlockDiagramModelAndInputAllAspectsInDirTest() {
		//dcmortorNONCAUSALinputTest()
		ArrayList<String> arrModelica = fileio.input(modelicaaspectpath+DCmortorModel);//BlockDiagramModel
		String first = arrModelica.get(0);
		String last  = arrModelica.get(arrModelica.size()-1);
		assertThat(first,is("model DCmortorNONCAUSAL"));
		assertThat(last,is("end DCmortorNONCAUSAL;"));
		
		//inputAllAspectsInDirTest()
		ArrayList<String> aspects = scp.findFile(modelicablockpath); //exists Aspect model
		for(int i=0;i<aspects.size();i++) {
			String srchAspect = aspects.get(i);
			if(srchAspect.contains("BlockDiagramAspect3")) {
				ArrayList<String> aspect1 = fileio.input(modelicablockpath+srchAspect);
				String[] result = aspect1.get(1).split("//",0);
				assertThat(result[0], is("aspect BlockDiagramAspect3"));
			} 
			if(srchAspect.contains("DCmotorAspect5.mo")) {
				ArrayList<String> aspect2 = fileio.input(modelicablockpath+srchAspect);
				String[] result = aspect2.get(0).split("//",0);
				assertThat(result[0], is("aspect DCmotorAspect5"));
			}
		}
	}
	
	/**
	 * 特定のディレクトリに存在するファイルの名前が○○Aspectとなっているすべてのファイルを読み込む
	 */
	@Test
	public void searchAspectsAllFileInputFile() {
		ArrayList<String> aspects = scp.findFile(modelicablockpath);
		ArrayList<String> aspectfiles = new ArrayList<String>();
		for(int i=0;i<aspects.size();i++) {
			String srchAspect = aspects.get(i);
			ArrayList<String> aspect = fileio.input(modelicablockpath+srchAspect);
			aspectfiles.add("**********************************************************************************************************************************");
			aspectfiles.addAll(aspect);
		}
		for(int i=0;i<aspectfiles.size();i++) {
			String srchAspect = aspectfiles.get(i);
			if(srchAspect.contains("aspect BlockDiagramAspect3")) {
				String[] result = srchAspect.split("//",0);
				assertThat(result[0], is("aspect BlockDiagramAspect3"));
			} 
			if(srchAspect.contains("aspect DCmotorAspect5")) {
				String[] result = srchAspect.split("//",0);
				assertThat(result[0], is("aspect DCmotorAspect5"));
			}
		}
	}


	/**
	 *  ポイントカットからモデル名を取り出す
	 * ブロック線図モデルに取り出したモデル名のモデルが存在する確認
	 * スコープクラスにてポイントカットArrayListを受け取れるメソッドをそれぞれ作成する
	 * ＊はここでは無視
	 */
	@Test
	public void confirmBlockDiagramModel() {
		ArrayList<String> convertPC = new ArrayList<String>();
		ArrayList<String> retrofitPC = new ArrayList<String>();
		
		ArrayList<String> arrInBlock = fileio.input(modelicaaspectpath+"DCmortorControll.mo");
		
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			if(convertAspect.size() != 0) {
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				//ArrayList<String> convertInstanceSection = interpret.interpretInstanceSection(convertAspect);
				convertAdviceSection = interpret.syntaxCheck(convertAdviceSection,advicePattern);
				convertPC = scp.createConvertSyntaxFromAdivce(convertAdviceSection,arrInBlock);
				//check(convertPC);
			} else if(retrofitAspect.size() != 0) {
				retrofitAspect = interpret.fileCheck(retrofitAspect, "advice");
				ArrayList<String> retrofitAdviceSection = interpret.interpretAdviceSection(retrofitAspect);
				//ArrayList<String> retrofitInstanceSection = interpret.interpretInstanceSection(retrofitAspect);
 				retrofitAdviceSection = interpret.syntaxCheck(retrofitAdviceSection,advicePattern);
				//retrofitPC = scp.createRetrofitSyntaxFromAdvice(retrofitPointCut);
				//check(retrofitPC);
			}
			
			assertNotNull(convertPC);
			assertNotNull(retrofitPC);
		}
	}
	
	/**
	 * ポイントカットで指定されたモデル名のモデルを読み込むテスト
	 */
	@Test
	public void inputConfirmModel() {
		ArrayList<String> arrTest = new ArrayList<String>();
		arrTest.add("DCmortorNONCAUSAL:stepVoltage1:  replace(signalVoltage)annotation(PointCut=\"DCmortorNONCAUSAL stepVoltage1\");");
		arrTest.add("Modelica.Blocks.Sources.Step:mortor.flange_b:  link(angleSensor.flange)annotation(PointCut=\"* mortor.flange_b\");");
		arrTest.add("Modelica.Blocks.Continuous.PID:mortor.flange_b:  link(angleSensor.flange)annotation(PointCut=\"* mortor.flange_b\");");
		arrTest.add("Modelica.Blocks.Math.Feedback:mortor.flange_b:  link(angleSensor.flange)annotation(PointCut=\"* mortor.flange_b\");");
		arrTest.add("DCmortorNONCAUSAL:mortor.flange_b:  link(angleSensor.flange)annotation(PointCut=\"* mortor.flange_b\");");
		
		for(String path :arrTest) {
			ArrayList<String> arrResult = scp.inputModel(path);
			//check(arrResult);
			assertNotNull(arrResult);
		}
	}
	
	/**
	 * 読み込んだパッケージファイルからあるモデルを取り出す
	 */
	@Test
	public void selectModelFromLib() {
		String modelicalibpath = "C:\\OMDev\\tools\\msys\\home\\watanabe\\OpenModelica\\build\\lib\\omlibrary\\Modelica 3.2.2";
		ArrayList<String> arrTest = new ArrayList<String>() ;
		arrTest.addAll(fileio.input(modelicalibpath+"\\Blocks\\Math.mo"));
		
		ArrayList<String> arrResult = scp.interpretModelInstanceSection(arrTest,"Feedback");
		//check(arrResult);
		assertNotNull(arrResult);
	}
	
	/**
	 * 正しい正規表現を使おうの会
	 */
	@Test
	public void regextest() {
		assertTrue("  block Feedback \"Output difference between commanded and feedback input\"".matches(
				"\\s*[A-Za-z0-9_]+\\sFeedback\\s[a-zA-Z0-9_\"\"\\s]*"));
		assertTrue(("\"Output difference between commanded and feedback input\"").matches(
				"\"[A-Za-z0-9_\\s]+\""));
		assertTrue("mortor.flange_b.a.a".matches(
				"[a-zA-Z0-9_]+\\.[a-zA-Z0-9_\\.]+"));
		assertTrue("  Modelica.Electrical.Analog.Basic.Resistor R annotation(".matches(
				"\\s*[a-zA-Z0-9_\\.]+ R [a-zA-Z0-9_\\.\\(]*"));
		assertTrue("".matches(""));
		assertTrue("      Rotational.Interfaces.Flange_b flange_b \"Right flange of shaft\"".matches(
				"\\s*[a-zA-Z0-9_\\.]+\\sflange_b\\s\"Right flange of shaft\""));
		assertTrue("model DCmortorControll".matches("\\s*[a-zA-Z0-9_]+\\sDCmortorControll"));
		
		assertTrue("  Modelica.Mechanics.Rotational.Sensors.AngleSensor angleSensor;".matches("\\s*[[a-zA-Z0-9_]*.]*[a-zA-Z0-9_]+\\s[a-zA-Z0-9_]+;"));
		
	//"\\s*[a-zA-Z0-9_\\.]+\\sflange_b\\s[a-zA-Z0-9_\\.\\(\"\"]*"));
	}
	
	/**
	 * 取り出したモデルにおいてある部品が存在するか探索し存在した場合新たな構文を作成する
	 */
	@Test
	public void createSearchSyntax() {
		ArrayList<String> arrTest = new ArrayList<String>();
		arrTest.add("DCmortorNONCAUSAL:stepVoltage1:  replace(signalVoltage)annotation(PointCut=\"DCmortorNONCAUSAL stepVoltage1\");");
		arrTest.add("Modelica.Blocks.Sources.Step:mortor.flange_b:  link(angleSensor.flange)annotation(PointCut=\"* mortor.flange_b\");");
		arrTest.add("Modelica.Blocks.Continuous.PID:mortor.flange_b:  link(angleSensor.flange)annotation(PointCut=\"* mortor.flange_b\");");
		arrTest.add("Modelica.Blocks.Math.Feedback:mortor.flange_b:  link(angleSensor.flange)annotation(PointCut=\"* mortor.flange_b\");");
		arrTest.add("DCmortorNONCAUSAL:mortor.flange_b:  link(angleSensor.flange)annotation(PointCut=\"* mortor.flange_b\");");
		
		
		for(String path :arrTest) {
			ArrayList<String> arrResult = scp.inputModel(path);
			//check(arrResult);
			assertNotNull(arrResult);
			if(arrResult.size() >0) {
				assertTrue(arrResult.get(0).split(":")[1].equals("><"));
			}
		}
	
	}
	
	/**
	 * イナーシャのモデルを読み込んでパスの末端まで繰り返すテスト
	 */
	@Test
	public void inputInertiaTest() {
		ArrayList<String> arrTest = new ArrayList<String>();
		arrTest.add("Modelica.Mechanics.Rotational.Components.Inertia:flange_b:  link(angleSensor.flange)annotation(PointCut=\"* mortor.flange_b\");");
		for(String path :arrTest) {
			ArrayList<String> arrResult = scp.inputModel(path);
			//arrResult = scp.createSearchSyntaxFromSyntax(path,arrResult);
			//check(arrResult);			
			
			assertNotNull(arrResult);
		}
	}
	
	@Test
	public void convertSearch() {
		ArrayList<String> convertPC = new ArrayList<String>();
		ArrayList<String> retrofitPC = new ArrayList<String>();
		
		ArrayList<String> arrInBlock = fileio.input(modelicaaspectpath+"DCmortorControll.mo");
		
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			if(convertAspect.size() != 0) {
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				convertAdviceSection = interpret.syntaxCheck(convertAdviceSection,advicePattern);
				convertPC = scp.createConvertSyntaxFromAdivce(convertAdviceSection,arrInBlock);
				ArrayList<String> arrPath = new ArrayList<String>();
				for(String path :convertPC) {
					arrPath.addAll(scp.inputModel(path));
					assertNotNull(arrPath);
					if(arrPath.size() >0) {
						assertTrue(arrPath.get(0).split(":")[1].equals("><"));
					}
				}
				//check(arrPath);
			} else if(retrofitAspect.size() != 0) {
				retrofitAspect = interpret.fileCheck(retrofitAspect, "advice");
				ArrayList<String> retrofitAdviceSection = interpret.interpretAdviceSection(retrofitAspect);
 				retrofitAdviceSection = interpret.syntaxCheck(retrofitAdviceSection,advicePattern);
				retrofitPC = scp.createRetrofitSyntaxFromAdvice(retrofitAdviceSection,arrInBlock);
				ArrayList<String> arrPath = new ArrayList<String>();
				for(String path :retrofitPC) {
					arrPath.addAll(scp.inputModel(path));
					assertNotNull(arrPath);
					if(arrPath.size() >0) {
						assertTrue(arrPath.get(0).split(":")[1].equals("><"));
					}
				}
				//check(arrPath);
			}
			
			assertNotNull(convertPC);
			assertNotNull(retrofitPC);
		}
	}
	
	
	@Test
	public void retrofitSearch() {
		ArrayList<String> convertPC = new ArrayList<String>();
		ArrayList<String> retrofitPC = new ArrayList<String>();
		
		ArrayList<String> arrInBlock = fileio.input(modelicaaspectpath+"DCmortorControll.mo");
		
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			if(convertAspect.size() != 0) {
			
			} else if(retrofitAspect.size() != 0) {
				retrofitAspect = interpret.fileCheck(retrofitAspect, "advice");
				ArrayList<String> retrofitAdviceSection = interpret.interpretAdviceSection(retrofitAspect);
 				retrofitAdviceSection = interpret.syntaxCheck(retrofitAdviceSection,advicePattern);
				retrofitPC = scp.createRetrofitSyntaxFromAdvice(retrofitAdviceSection,arrInBlock);
				ArrayList<String> arrPath = new ArrayList<String>();
				//check(retrofitPC);
				for(String path :retrofitPC) {
					arrPath.addAll(scp.inputModel(path));
					assertNotNull(arrPath);
					if(arrPath.size() >0) {
						assertTrue(arrPath.get(0).split(":")[1].equals("><"));
					}
				}
				//check(arrPath);
			}
			
			assertNotNull(convertPC);
			assertNotNull(retrofitPC);
		}
	}
	
	/**
	 * ファイル探索構文からアドバイス部分を抜き取る
	 */
	@Test
	public void lastTest() {
		ArrayList<String> convertSyntax = new ArrayList<String>();
		ArrayList<String> retrofitSyntax = new ArrayList<String>();
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		
		ArrayList<String> arrInBlock = fileio.input(modelicaaspectpath+"DCmortorControll.mo");
		
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		for(String aspectFileName : aspectFileNames) {
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			if(convertAspect.size() != 0) {
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				ArrayList<String> convertInstanceSection = interpret.interpretInstanceSection(convertAspect);
				convertAdviceSection = interpret.syntaxCheck(convertAdviceSection,advicePattern);
				convertSyntax = scp.createConvertSyntaxFromAdivce(convertAdviceSection,arrInBlock);
				ArrayList<String> searched = new ArrayList<String>();
				for(String path :convertSyntax) {
					searched.addAll(scp.inputModel(path));
				}
				searched = scp.createAdviceSyntax(searched);
				convertAdvice = interpret.interpretAdvice(searched,convertInstanceSection);
				check(convertAdvice);
			} else if(retrofitAspect.size() != 0) {
				retrofitAspect = interpret.fileCheck(retrofitAspect, "advice");
				ArrayList<String> retrofitAdviceSection = interpret.interpretAdviceSection(retrofitAspect);
				ArrayList<String> retrofitInstanceSection = interpret.interpretInstanceSection(retrofitAspect);
 				retrofitAdviceSection = interpret.syntaxCheck(retrofitAdviceSection,advicePattern);
				retrofitSyntax = scp.createRetrofitSyntaxFromAdvice(retrofitAdviceSection,arrInBlock);
				ArrayList<String> searched = new ArrayList<String>();
				for(String path :retrofitSyntax) {
					searched.addAll(scp.inputModel(path));
				}
				searched = scp.createAdviceSyntax(searched);
				retrofitAdvice = interpret.interpretAdvice(searched,retrofitInstanceSection);
				check(retrofitAdvice);
			}
			
			assertNotNull(convertSyntax);
			assertNotNull(retrofitSyntax);
		}
	}

}
