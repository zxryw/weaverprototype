package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import weaver.Scope;

public class etcTest {
	
	Scope scp;

	@Before
	public void setUp() throws Exception {
		scp = new Scope();
	}
	
	public void check(ArrayList<String> arrList) {
		for(int i=0;i<arrList.size();i++) {
			System.out.println(arrList.get(i));
		}
	}

	@Test
	public void test1() {
		ArrayList<String>convertSrch =new ArrayList<String>();
		//convertSrch.add("Modelica.Blocks.Continuous.PID:stepVoltage1:  replace(signalVoltage)annotation(PointCut=\"* stepVoltage1\");22");
		convertSrch.add("DCmortorControll:PID.y:  output(dcmotor.y)annotation(PointCut=\"DCmortorControll PID.y\");");
		ArrayList<String> convertSrchSyntax = new ArrayList<String>();
		for(String path :convertSrch) {
			convertSrchSyntax.addAll(scp.inputModel(path));
		}
		check(convertSrchSyntax);
	}

}
