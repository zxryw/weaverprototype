package test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import weaver.FileIO;
import weaver.Interpreter;
import weaver.Scope;

public class FileIOAndInterpreterJoinTest {
	
	//インスタンス
	private FileIO fileio;
	private Scope  scp;
	private Interpreter interpret;
	
	//パス
	private String modelicaaspectpath = "C:\\modelica\\Aspect\\";
	private String modelicablockpath= "C:\\modelica\\blockdiagram\\";
	private String modelicatestpath = "C:\\modelica\\weavertest\\";
	
	//ファイル名
	private String DCmortorModel = "DCmortorNONCAUSAL.mo";
	private String outModelicaFile6 = "DCmortorCAUSAL2.mo";
	
	//正規表現パターン
	private String aspectPattern ="aspect\\s[a-zA-Z0-9]*";
	private String annotationPattern = "annotation\\(Aspect=\"[a-zA-Z]*\"\\);";
	private String pointcutPattern = "[[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\s[[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_]+";
	private String advicePattern ="\\s*[a-z]*\\([[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\)annotation\\(PointCut=\""+pointcutPattern +"\"\\);";

	@Before
	public void setUp() throws Exception {
		fileio = new FileIO();
		scp = new Scope();
		interpret = new Interpreter();
	}
	
	public void check(ArrayList<String> arrList) {
		for(int i=0;i<arrList.size();i++) {
			System.out.println(arrList.get(i));
		}
	}

	/**
	 * 実際にファイルを読み込んで解析クラスで構文を生成しウィービングを行う
	 */
	@Test
	public void fileioAndInterpreterJoinTest() {
		
		
		
		//1.完成させるべきブロック線図モデルを読み込む
		ArrayList<String> arrInBlock = fileio.input(modelicaaspectpath+"DCmortorControll.mo");
		
		//2.ディレクトリに存在する全アスペクトの名前を列挙
		ArrayList<String> aspectFileNames = scp.findFile(modelicablockpath);//本来ならブロック線図モデルが存在するディレクトリにおいて探索を行う
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		//3.列挙したアスペクトファイル名から一つずつアスペクトファイル名を選択して処理を行う
		for(String aspectFileName : aspectFileNames) {
			//4.アスペクトファイル名からアスペクトファイルを読み込む
			ArrayList<String> inputAspect = fileio.input(modelicablockpath+aspectFileName);
			//5.ファイルチェック１：ファイルの中身において"aspect 〇〇"の記述がなされているか確認
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			//6.ファイルチェック２：ファイルの中身において"annotation(Aspect="..")；"の記述がなされているか確認（いらない？）
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			//7.C.単独アノテーションにおいてアスペクトの種類を区別。ここではConvertAspectを区別
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			//7.R.単独アノテーションにおいてアスペクトの種類を区別。ここではRetrofitAspectを区別
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			if(convertAspect.size() != 0) {//8.C.アスペクトが変換アスペクトだった場合
				//9.C.ファイルチェック３：ファイルの中身において"advice"セクションが存在するか確認
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				//10.C.アドバイスセクションを抜き出す
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				//11.C.インスタンスセクションを抜き出す
				ArrayList<String> convertInstanceSection = interpret.interpretInstanceSection(convertAspect);
				//12.C.アドバイス文が文法通り記述されているかチェック
				convertAdviceSection = interpret.syntaxCheck(convertAdviceSection,advicePattern);
				//13.C.ポイントカットを抜き出す
				//14.C.モデリカ構文を生成
				convertAdvice = interpret.interpretAdvice(convertAdviceSection,convertInstanceSection);
			} else if(retrofitAspect.size() != 0) {//8.R.アスペクトが組み込みアスペクトだった場合s
				retrofitAspect = interpret.fileCheck(retrofitAspect, "advice");
				ArrayList<String> retrofitAdviceSection = interpret.interpretAdviceSection(retrofitAspect);
				ArrayList<String> retrofitInstanceSection = interpret.interpretInstanceSection(retrofitAspect);
 				retrofitAdviceSection = interpret.syntaxCheck(retrofitAdviceSection,advicePattern);
				retrofitAdvice = interpret.interpretAdvice(retrofitAdviceSection,retrofitInstanceSection);
			}
		}
		
		//15.ブロック線図モデルから非因果制御対象モデルを探し出す
		//16.非因果制御対象モデルを読み込む
		ArrayList<String> arrIn = fileio.input(modelicaaspectpath+DCmortorModel);
		//17.インスタンスセクションを特定
		ArrayList<Integer> indxs = fileio.searchKeyword(arrIn, "model DCmortorNONCAUSAL");
		//18.インスタンスの生成構文を挿入
		for(String instance : convertAdvice) {
			if(instance.contains("RI:")) {
				//19．アクチュエータへの置換
				String[] instDivine = instance.split("\\s");
				String[] instName = instDivine[3].split(";");//アスペクトで生成したほうの名前
				ArrayList<Integer> instIndxs = fileio.searchKeyword(arrIn, instName[0]);
				String instBefore = arrIn.get((instIndxs.get(0))).split("\\s")[2];
				String instAfter = instDivine[2];
				arrIn = fileio.replaceKeyword(arrIn,instIndxs,instAfter,instBefore);
			}else if(instance.contains("I:")) {
				String[] instComp = instance.split(":");
				arrIn = fileio.writeString(arrIn,instComp[1], indxs.get(0)+1);
			}
		}
		//20.connect文を挿入
		indxs = fileio.searchKeyword(arrIn, "equation");
		for(String instance : convertAdvice) {
			if(instance.contains("A:")) {
				String[] instComp = instance.split(":");
				arrIn = fileio.writeString(arrIn,instComp[1], indxs.get(0)+1);
			}
		}
		//21．モデル名をファイル名に変更する
		indxs = fileio.searchKeyword(arrIn, "DCmortorNONCAUSAL");
		arrIn = fileio.replaceKeyword(arrIn, indxs, "DCmortorCAUSAL2", "DCmortorNONCAUSAL");
		//22．非因果制御対象モデルにファイルを出力
		ArrayList<String> arrOut = fileio.output(arrIn, modelicatestpath+outModelicaFile6);
		assertNotNull(arrOut);
		
		//23.非因果制御対象モデルの宣言を変換した因果制御対象モデルに変更する
		ArrayList<Integer> indxsBlock = fileio.searchKeyword(arrInBlock, "DCmortorNONCAUSAL");
		arrInBlock = fileio.replaceKeyword(arrInBlock, indxsBlock, "DCmortorCAUSAL2", "DCmortorNONCAUSAL");
		//24．制御対象モデルを接続するためのconnect文を挿入
		indxsBlock = fileio.searchKeyword(arrInBlock, "equation");
		for(String equation : retrofitAdvice) {
			if(equation.contains("A:")) {
				String[] instComp = equation.split(":");
				arrInBlock = fileio.writeString(arrInBlock,instComp[1], indxsBlock.get(0)+1);
			}
		}
		//25.モデル名をファイル名に変更する
		indxs = fileio.searchKeyword(arrInBlock, "DCmortorControll");
		arrInBlock = fileio.replaceKeyword(arrInBlock, indxs, "DCmortorControllComplete2", "DCmortorControll");
		//26.ファイルを出力
		ArrayList<String> arrOutBlock = fileio.output(arrInBlock, modelicatestpath+"DCmortorControllComplete2.mo");
		
		check(arrOutBlock);
		
		assertThat(arrInBlock.get(0),is("model DCmortorControllComplete2"));
		assertThat(arrIn.get(0),is("model DCmortorCAUSAL2"));
		
		
	}

}
