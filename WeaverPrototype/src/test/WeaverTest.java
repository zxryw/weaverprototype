package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import weaver.FileIO;
import weaver.Interpreter;
import weaver.Scope;

public class WeaverTest {

	private FileIO fileio;
	private Scope  scp;
	private Interpreter interpret;
	
	private String modelicanowpath = "C:\\modelica\\Now\\";
	
	private String aspectPattern ="aspect\\s[a-zA-Z0-9]*";
	private String annotationPattern = "annotation\\(Aspect=\"[a-zA-Z]*\"\\);";
	private String pointcutPattern = "[[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\s[[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_]+";
	private String advicePattern ="\\s*[a-z]*\\([[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\)annotation\\(PointCut=\""+pointcutPattern +"\"\\);";

	
	public void check(ArrayList<String> arrList) {
		for(int i=0;i<arrList.size();i++) {
			System.out.println(arrList.get(i));
		}
	}


	@Before
	public void setUp() throws Exception {
		this.scp = new Scope();
		this.fileio = new FileIO();
		this.interpret = new Interpreter();
	}

	/**
	 * スコープクラス、インタープリタークラス、ファイルIOクラスの結合テスト
	 */
	@Test
	public void JoinTest() {
		
		String nonCausalModel="";

		//1.完成させるべきブロック線図モデルを読み込む
		ArrayList<String> arrInBlock = fileio.input(modelicanowpath+"DCmortorControll.mo");
		
		//2.ディレクトリに存在する全アスペクトの名前を列挙
		ArrayList<String> aspectFileNames = scp.findFile(modelicanowpath);
		ArrayList<String> convertAdvice = new ArrayList<String>();
		ArrayList<String> retrofitAdvice = new ArrayList<String>();
		//3.列挙したアスペクトファイル名から一つずつアスペクトファイル名を選択して処理を行う
		for(String aspectFileName : aspectFileNames) {
			//4.アスペクトファイル名からアスペクトファイルを読み込む
			ArrayList<String> inputAspect = fileio.input(modelicanowpath+aspectFileName);
			//5.ファイルチェック１：ファイルの中身において"aspect 〇〇"の記述がなされているか確認
			ArrayList<String> rightAspect = interpret.fileCheck(inputAspect,aspectPattern);
			//6.ファイルチェック２：ファイルの中身において"annotation(Aspect="..")；"の記述がなされているか確認
			rightAspect = interpret.fileCheck(rightAspect,annotationPattern);
			
			//7.C.単独アノテーションにおいてアスペクトの種類を区別。ここではConvertAspectを区別
			ArrayList<String> convertAspect = interpret.interpretAnnotationAspectType(rightAspect,"ConvertAspect");
			//7.R.単独アノテーションにおいてアスペクトの種類を区別。ここではRetrofitAspectを区別
			ArrayList<String> retrofitAspect = interpret.interpretAnnotationAspectType(rightAspect, "RetrofitAspect");
			if(convertAspect.size() != 0) {//8.C.アスペクトが変換アスペクトだった場合
				//9.C.ファイルチェック３：ファイルの中身において"advice"セクションが存在するか確認
				convertAspect = interpret.fileCheck(convertAspect, "advice");
				//10.C.アドバイスセクションを抜き出す
				ArrayList<String> convertAdviceSection = interpret.interpretAdviceSection(convertAspect);
				//11.C.インスタンスセクションを抜き出す
				ArrayList<String> convertInstanceSection = interpret.interpretInstanceSection(convertAspect);
				//12.C.アドバイス文が文法通り記述されているかチェック
				convertAdviceSection = interpret.syntaxCheck(convertAdviceSection,advicePattern);
				//13.C.ポイントカットを抜き出す
				//ArrayList<String> convertPointCut = interpret.interpretPointCut(convertAdviceSection);
				//14.C.ファイル探索を行うための構文を作成
				ArrayList<String>convertPC = scp.createConvertSyntaxFromAdivce(convertAdviceSection,arrInBlock);
				ArrayList<String> arrPath = new ArrayList<String>();
				//15.C.構文一つ一つを評価していく
				for(String path :convertPC) {
					//16.C.構文を再帰的にファイルの探索を行い必要なものだけを入れる
					arrPath.addAll(scp.inputModel(path));
				}
				nonCausalModel = interpret.takeNonCausalModelName(arrPath);
				//17.C.ファイル探索構文をアドバイス構文に変換する
				ArrayList<String> searched = scp.createAdviceSyntax(arrPath);
				//18.C.アドバイスからモデリカ構文を生成する
				convertAdvice = interpret.interpretAdvice(searched,convertInstanceSection);
			} else if(retrofitAspect.size() != 0) {
				retrofitAspect = interpret.fileCheck(retrofitAspect, "advice");
				ArrayList<String> retrofitAdviceSection = interpret.interpretAdviceSection(retrofitAspect);
				ArrayList<String> retrofitInstanceSection = interpret.interpretInstanceSection(retrofitAspect);
 				retrofitAdviceSection = interpret.syntaxCheck(retrofitAdviceSection,advicePattern);
				//ArrayList<String> retrofitPointCut = interpret.interpretPointCut(retrofitAdviceSection);
				ArrayList<String> retrofitPC = scp.createRetrofitSyntaxFromAdvice(retrofitAdviceSection,arrInBlock);
				ArrayList<String> arrPath = new ArrayList<String>();
				for(String path :retrofitPC) {
					arrPath.addAll(scp.inputModel(path));
				}
				ArrayList<String> searched = scp.createAdviceSyntax(arrPath);
				retrofitAdvice = interpret.interpretAdvice(searched,retrofitInstanceSection);
			}
		}
		
		//15.ブロック線図モデルから非因果制御対象モデルを探し出す
		
		//16.非因果制御対象モデルを読み込む
		ArrayList<String> arrIn = fileio.input(modelicanowpath+nonCausalModel+".mo");
		//17.インスタンスセクションを特
		ArrayList<Integer> indxs = fileio.searchKeyword(arrIn, "model " + nonCausalModel);
		//18.インスタンスの生成構文を挿入
		for(String instance : convertAdvice) {
			if(instance.contains("RI:")) {
				//19．アクチュエータへの置換
				String[] instDivine = instance.split("\\s");
				String[] instName = instDivine[3].split(";");//アスペクトで生成したほうの名前
				ArrayList<Integer> instIndxs = fileio.searchKeyword(arrIn, instName[0]);
				String instBefore = arrIn.get((instIndxs.get(0))).split("\\s")[2];
				String instAfter = instDivine[2];
				arrIn = fileio.replaceKeyword(arrIn,instIndxs,instAfter,instBefore);
			}else if(instance.contains("I:")) {
				String[] instComp = instance.split(":");
				arrIn = fileio.writeString(arrIn,instComp[1], indxs.get(0)+1);
				}
		}
		//20.connect文を挿入
		indxs = fileio.searchKeyword(arrIn, "equation");
		for(String instance : convertAdvice) {
		if(instance.contains("A:")) {
				String[] instComp = instance.split(":");
				arrIn = fileio.writeString(arrIn,instComp[1], indxs.get(0)+1);
			}
		}
		//21．モデル名をファイル名に変更する
		indxs = fileio.searchKeyword(arrIn, nonCausalModel);
		arrIn = fileio.replaceKeyword(arrIn, indxs, nonCausalModel+"Weaved", nonCausalModel);
		//22．非因果制御対象モデルにファイルを出力
		ArrayList<String> arrOut = fileio.output(arrIn, modelicanowpath+nonCausalModel+"Weaved.mo");
		assertNotNull(arrOut);
				
		//23.非因果制御対象モデルの宣言を変換した因果制御対象モデルに変更する
		ArrayList<Integer> indxsBlock = fileio.searchKeyword(arrInBlock, nonCausalModel);
		arrInBlock = fileio.replaceKeyword(arrInBlock, indxsBlock, nonCausalModel+"Weaved", nonCausalModel);
		//24．制御対象モデルを接続するためのconnect文を挿入
		indxsBlock = fileio.searchKeyword(arrInBlock, "equation");
		for(String equation : retrofitAdvice) {
			if(equation.contains("A:")) {
				String[] instComp = equation.split(":");
				arrInBlock = fileio.writeString(arrInBlock,instComp[1], indxsBlock.get(0)+1);
			}
		}
		//25.モデル名をファイル名に変更する
		indxs = fileio.searchKeyword(arrInBlock, "DCmortorControll");
		arrInBlock = fileio.replaceKeyword(arrInBlock, indxs, "DCmortorControllComplete3", "DCmortorControll");
		//26.ファイルを出力
		ArrayList<String> arrOutBlock = fileio.output(arrInBlock, modelicanowpath+"DCmortorControllComplete3.mo");
		assertNotNull(arrOutBlock);
		
		check(arrIn);
	}

}
