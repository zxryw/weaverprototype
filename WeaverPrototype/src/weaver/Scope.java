package weaver;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * パスで指定されたファイルを探索したり、指定された部品がそのファイルに存在するか判別する
 * @author watanabe
 *
 */
public class Scope {
	
	private String instancePattern= "\\s*[[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\s[a-zA-Z0-9_]+\\s[a-zA-Z0-9_\\(\\)]*";
	private String modelicaLibPattern = "Modelica[.[a-zA-Z_0-9]+]+";
	
	private String modelicanowpath = "C:\\modelica\\Now\\";
	private String modelicalibpath = "C:\\OMDev\\tools\\msys\\home\\watanabe\\OpenModelica\\build\\lib\\omlibrary\\Modelica 3.2.2";
	
	private FileIO fileio = new FileIO();
	
	private int num = 0;
	
	
	/**
	 * 指定されたパスのファイルにおいてファイルを探索する（ここではAspectという名前を含むファイルのみ検索できる)
	 * @param path 探索ディレクトリまでのパス
	 * @return 探索に成功したファイル名列
	 */
	public ArrayList<String> findFile(String path) {
		File dir = new File(path);
		File[] files = dir.listFiles();
		ArrayList<String> aspects = new ArrayList<String>();
		
		for(int i = 0;i < files.length; i++) {
			File file = files[i];
			String[] dirOrFileName = (file.toString()).split("\\\\",0);
			int lastindx = dirOrFileName.length-1;
			
			if(dirOrFileName[lastindx].contains("Aspect")||dirOrFileName[lastindx].contains("aspect")) {
				aspects.add(dirOrFileName[lastindx]);
			}
		}
		return aspects;
	}

	/**
	 * 変換アスペクトにおいてファイル探索構文を生成する
	 * ファイル探索構文
	 * 見つかったモデル名:そのモデルに存在するであろうモデルパス:アドバイス本体
	 * @param advices 変換アスペクトのアドバイス列
	 * @param model　ブロック線図モデル
	 * @return　作成したファイル探索構文
	 */
	public ArrayList<String> createConvertSyntaxFromAdivce(ArrayList<String> advices, ArrayList<String> model) {
		ArrayList<String> convertSrchSyntax = new ArrayList<String>();
		
		for(String advice : advices) {
			String modelName = advice.split("\\s")[2].split("\"")[1];
			String path = advice.split("\\s")[3].split("\"")[0];
			if(modelName.equals("*")) {
				for(String blockDiagramLine : model) {
					if(blockDiagramLine.matches(instancePattern)) {
						//TODO 空白込みで区分を行っている
						String[] words = blockDiagramLine.split("\\s");
						ArrayList<String> adv = new ArrayList<String>();
						adv.add(advice);
						ArrayList<Integer> indxs = new ArrayList<Integer>();
						indxs.add(0);
						adv = fileio.replaceKeyword(adv, indxs, words[2], "*");//アスタリスクを持つアドバイスに対してアスタリスクを挿入先のモデルに変更する
						convertSrchSyntax.add(words[2] + ":" + path + ":" +adv.get(0));
					}
				}
			}else {
				for(String blockDiagramLine : model) {
					if(blockDiagramLine.contains(modelName)) {//変換アスペクトの場合はインスタンスセクションにおいて定義されていることが必要
						// TODO　この部分以外はcreateRetrofitSyntaxFromAdviceと一つにできるのでは
						convertSrchSyntax.add(modelName + ":" + path + ":" +  advice);
					}
				}
			}
		}
		return convertSrchSyntax;
	}

	/**
	 * 組み込みアスペクトにおいてファイル探索構文を生成する
	 * @param advices 組み込みアスペクトのアドバイス列
	 * @param model　ブロック線図モデル
	 * @return 作成したファイル探索構文
	 */
	public ArrayList<String> createRetrofitSyntaxFromAdvice(ArrayList<String> advices,ArrayList<String> model) {
		ArrayList<String> retrofitSrchSyntax = new ArrayList<String>();
		
		for(String advice : advices) {
			String modelName = advice.split("\\s")[2].split("\"")[1];
			String path = advice.split("\\s")[3].split("\"")[0];
			if(modelName.equals("*")) {
				for(String blockDiagramLine : model) {
					if(blockDiagramLine.matches(instancePattern)) {
						//TODO 空白込みで区分を行っている
						String[] words = blockDiagramLine.split("\\s");
						// TODO この段階で*を含むポイントカットがある場合はどのモデルに挿入するか明確にするため*を導入先のモデル名に変更したほうが良い
						//そうしないと一つのブロック線図に複数の制御対象が存在した場合にどのアスペクトをどのモデルに織り込んでいいかわからなくなる
						retrofitSrchSyntax.add(words[2] + ":" + path + ":" +advice);
					}
				}
			}else {
				for(String blockDiagramLine : model) {
					if(blockDiagramLine.matches("\\s*[a-zA-Z0-9_]+\\s"+modelName)) {//接続アスペクトの場合は読み込んでいるブロック線図モデルそのものの名前が必要
						// TODO　この部分以外はcreateCOnvertSyntaxFromAdviceと一つにできるのでは
						retrofitSrchSyntax.add(modelName + ":" + path + ":" +  advice);
					}
				}
			}
		}
		return retrofitSrchSyntax;
	}

	/**
	 * 指定されたモデルを読み込む
	 * @param path モデリカライブラリに存在する部品のパス
	 * @return　うんこ
	 */
	public ArrayList<String> inputModel(String path) {
		ArrayList<String> srchSyntaxes = new ArrayList<String>();
		String modelName = path.split(":")[0];

		if(modelName.matches(modelicaLibPattern)) {
			String[] dirs = modelName.split(Pattern.quote("."));
			String modelPath = modelicalibpath;
			for(int i=1;i<dirs.length-1;i++) {
				modelPath = modelPath+"\\" + dirs[i];
				// TODO Rotationalだけじゃないよね
				if(dirs[i].equals( "Rotational")) {//Rotationalの場合はその中にさらにパッケージが存在している
					break;
				}
			}
			srchSyntaxes.addAll(interpretModelInstanceSection(fileio.input(modelPath+ ".mo"),dirs[dirs.length-1]));
		} else {
			srchSyntaxes.addAll(interpretModelInstanceSection(fileio.input(modelicanowpath+modelName+".mo"),modelName));
		}

		// TODO これやめませんか
		//System.out.println(num++ + "input");
		return createSearchSyntaxFromSyntax(path,srchSyntaxes);
	}
	
	/**
	 * アスペクトではないモデルにおけるインスタンスセクションのみを取り出す
	 * @param model
	 * @param modelName
	 * @return
	 */
	//TODO interpreterクラスのメソッドと一緒にできないか ||を使おう
	public ArrayList<String> interpretModelInstanceSection(ArrayList<String> model,String modelName) {
		ArrayList<String> instances = new ArrayList<String>();
		boolean output = false;
		for(int i=0;i<model.size();i++) {
			String srchAspectLine = model.get(i);
			if(srchAspectLine.matches("\\s*[[A-Za-z0-9_]\\s]*[A-Za-z0-9_]+\\s"
					+ modelName
					+ "\\s*[a-zA-Z0-9_\"\"\\s\\-]*")) {
				
				output = true;
			}
			if(srchAspectLine.matches("\\s*equation")) {
				output = false;
			}
			if(srchAspectLine.matches("\\s*end\\s[A-Za-z0-9_]+;")) {
				output = false;
			}
			if(output) {
				instances.add(srchAspectLine);
			}
		}
		return instances;
	}

	/**
	 * ファイル探索構文から新たなファイル探索構文を生成する
	 * @param oldSyntax　ファイル探索構文
	 * @param model　探索モデル
	 * @return　新たに生成されたファイル探索構文
	 */
	public ArrayList<String> createSearchSyntaxFromSyntax(String oldSyntax, ArrayList<String> model) {
		ArrayList<String> createSyntax = new ArrayList<String>();
		String searchModel = oldSyntax.split(":")[1].split(Pattern.quote("."))[0];
		if(searchModel.equals("><"))createSyntax.add(oldSyntax);
		String advice = oldSyntax.split(":")[2];
		String newPath = oldSyntax.split(":")[1];
		String newPathExtends = oldSyntax.split(":")[1];
		if(newPath.matches("[a-zA-Z0-9_]+\\.[a-zA-Z0-9_\\.]+")) {//a.b.cの場合
			newPath = newPath.split(Pattern.quote("."))[1];
		}else {//それ以外の場合
			newPath = "><";
		}
		
		for(String modelLine : model) {
			// Iconsには図形に関するアノテーションしかないので無視してよい
			if(modelLine.contains("extends")&& !modelLine.contains("Icons")) {//extendsが含まれていたらそのモデルも探索を行う
				// TODO extendsのパスがモデルによって異なる場合があるのでそこの処理
				// TODO extendsで潜っていってもし潜った先に探し求めているモデルが存在しなかったときここの処理はどうなるのかわからなん
				String extendsPath = modelLine.split("extends",0)[1].split(";")[0].split("\\s")[1].split("\\(")[0];//extendsで指定されたパス
				String modelica = oldSyntax.split(":")[0].split(Pattern.quote("."))[0];
				String down = oldSyntax.split(":")[0].split(Pattern.quote("."))[1];
				extendsPath = modelica+"."+down+"."+extendsPath;
				//extendsが発見された場合はその中身についても探索する必要があるのでnewPath部分が><だと困る
				//System.out.println(num++ +"extends("+extendsPath+")");
				return inputModel(extendsPath+":"+newPathExtends+":"+advice);
			}
			if(modelLine.matches("\\s*[[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\s"
					+ searchModel
					+"\\s*[a-zA-Z0-9_\\.\\(\\)\"\"\\s={,-]*")) {//探索が終わったら探索構文を挿入
				String newSyntax = modelLine.split("\\s")[2]+":"+newPath+":"+advice;
				//System.out.println(num++ + "find");
				createSyntax.add(newSyntax);
			}
		}
		if(!newPath.equals("><")&&createSyntax.size()>0) {//探索が終わっていないならinputModelを呼び出しさらに探索
			//System.out.println(num++ + "not ><:" + createSyntax.get(0));
			return inputModel(createSyntax.get(0));
		}
		//見つからなかったらからを返す
		//if(createSyntax.size()>0) System.out.println(num++ +"return:"+createSyntax.get(0));
		return createSyntax;
	}

	
	/**
	 * ファイル探索構文からアドバイス構文を生成する
	 * @param searched　ファイル探索構文列
	 * @return　生成されたアドバイス列
	 */
	public ArrayList<String> createAdviceSyntax(ArrayList<String> searched) {
		ArrayList<String> advices = new ArrayList<String>();
		
		for(String syntax : searched) {
			advices.add(syntax.split(":")[2]);
		}
		
		return advices;
	}
	
	/**
	 * 再帰的な呼び出しを行わない一般的なInputModel
	 * @param path
	 * @return
	 */
	// TODO 一つにしましょう
	public ArrayList<String> publicInputModel(String path) {
		ArrayList<String> arrInputModel = new ArrayList<String>();
		String modelName = path.split(":")[0];

		if(modelName.matches(modelicaLibPattern)) {
			String[] dirs = modelName.split(Pattern.quote("."));
			String modelPath = modelicalibpath;
			for(int i=1;i<dirs.length-1;i++) {
				modelPath = modelPath+"\\" + dirs[i];
				if(dirs[i].equals( "Rotational")) {
					break;
				}
			}
			arrInputModel.addAll(interpretModelInstanceSection(fileio.input(modelPath+ ".mo"),dirs[dirs.length-1]));
		} else {
			arrInputModel.addAll(interpretModelInstanceSection(fileio.input(modelicanowpath+modelName+".mo"),modelName));
		}
		return arrInputModel;
	}
}
