package weaver;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * アスペクト記述ルールに沿っているか確認したり、構文の解析を行う（ファイルの解析は行わない）
 * @author watanabe
 *
 */
public class Interpreter {
	
	private int numU =0;
	private int numY =0;
	private String takeResult ="";
	
	private String pointcutPattern ="[[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\s[[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+";
	private String instancePattern = "\\s*[[a-zA-Z0-9_]*.]*[a-zA-Z0-9_]+\\s[a-zA-Z0-9_]+;";
	private String singleAnnPattern ="annotation\\(AspectType=\"[a-zA-Z]*\"\\);";
	private String advicePattern ="\\s*[a-z]*\\([[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\)annotation\\(PointCut=\""
									+pointcutPattern 
									+"\"\\);";
	private String replacePattern = "\\s*replace\\([a-zA-Z0-9_\\*]*\\)annotation\\(PointCut=\""
										+ pointcutPattern
										+ "\"\\);";
	private String addPattern = "\\s*add\\([[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\)annotation\\(PointCut=\""
									+pointcutPattern
									+"\"\\);";
	private String insertPattern = "\\s*insert\\([[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\)annotation\\(PointCut=\""
									+pointcutPattern
									+"\"\\);";
	private String linkPattern = "\\s*link\\([[a-zA-Z0-9_\\*]*.]*[a-zA-Z0-9_\\*]+\\)annotation\\(PointCut=\""
									+pointcutPattern
									+"\"\\);";
	

	/**
	 * ファイルがルールによる記述ができているか判断できる（ファイルに指定した１文が存在した時点でOK）
	 * @param aspect チェック対象
	 * @param regex　記述ルール
	 * @return　結果
	 */
	public ArrayList<String> fileCheck(ArrayList<String> aspect, String regex) {
		boolean output = false;
		
		for(int i=0;i<aspect.size();i++) {
			String srchAspectLine = aspect.get(i);
			srchAspectLine = srchAspectLine.split("//",0)[0];
			// TODO:↑今探索行に//(コメント）があった場合を想定してはじいているが""とか()があった場合はどうするつもりなのか
			if(srchAspectLine.matches(regex)) {
				output = true;
				break;
			}
		}
		if(output)return aspect;
//		String aspectfirst = "";
//		if(aspect.size()>0)aspectfirst = aspect.get(0);
//		System.out.println("ファイルチェックに失敗しました:::"+regex+":::"+aspectfirst);
		return new ArrayList<String>();
	}
	
	/**
	 * アスペクトの単独アノテーションから変換アスペクトか接続アスペクトか判断するs
	 * @param aspect　判断対象
	 * @param aspectType　アスペクトのタイプ
	 * @return　結果
	 */
	public ArrayList<String> interpretAnnotationAspectType(ArrayList<String> aspect, String aspectType){
		ArrayList<String> resultAspects = new ArrayList<String>();
		for(int i=0;i<aspect.size();i++) {
			String srchAspectLine = aspect.get(i);
			srchAspectLine = srchAspectLine.split("//",0)[0].split("\\s")[2];
			// TODO:↑今探索行に//(コメント）があった場合を想定してはじいているが""とか()があった場合はどうするつもりなのか
			// TODO:リファクタリング候補
			if(srchAspectLine.matches(singleAnnPattern)) {
				srchAspectLine = srchAspectLine.split("\"", 0)[1];
				if(aspectType.equals(srchAspectLine)) {
					resultAspects.addAll(aspect);
				}
				break;
			}
		}
		return resultAspects;
	}
	
	/**
	 * アスペクトファイル内に存在するアドバイスセクションを抜き出す
	 * @param aspect 抜き出し対象
	 * @return　抜き出されたアドバイスセクション
	 */
	public ArrayList<String> interpretAdviceSection(ArrayList<String> aspect) {
		ArrayList<String> advices = new ArrayList<String>();
		for(int i=0;i<aspect.size();i++) {
			String srchAspectLine = aspect.get(i);
			srchAspectLine = srchAspectLine.split("//",0)[0];
			// TODO:↑今探索行に//(コメント）があった場合を想定してはじいているが""とか()があった場合はどうするつもりなのか
			// TODO:リファクタリング候補
			if(srchAspectLine.matches(advicePattern)) {
				// TODO:先頭の空白を処理せずに追加している。良くない。しかし先頭の空白込みで後の処理も書いてある
				advices.add(srchAspectLine);
			}
		}
		return advices;
	}
	
	/**
	 * アスペクトファイル内に存在するインスタンスセクションを抜き出す
	 * @param aspect　抜き出し対象
	 * @return　抜き出されたインスタンスセクション
	 */	
	public ArrayList<String> interpretInstanceSection(ArrayList<String> aspect) {
		ArrayList<String> instances = new ArrayList<String>();
		for(int i=0;i<aspect.size();i++) {
			String srchAspectLine = aspect.get(i);
			srchAspectLine = srchAspectLine.split("//",0)[0];
			if(srchAspectLine.matches("advice")) {
				return instances;
			}
			if(srchAspectLine.matches(instancePattern)) {
				instances.add(srchAspectLine);
			}
		}
		return instances;
	}

	/**
	 * アドバイス構文を解釈しモデリカ構文を返す
	 * @param advice　アドバイス構文列
	 * @param instance　アドバイスのインスタンス生成列
	 * @return　生成されたモデリカ構文列
	 */
	public ArrayList<String> interpretAdvice(ArrayList<String> advice, ArrayList<String> instances) {
		ArrayList<String> interpretedAdvice = new ArrayList<String>();
		//TODO アドバイスのコネクタ指定においても*を使用できるようにする
		//TODO パスにおいてコネクタの部分は*はだめ
		
		for(int i =0;i<advice.size();i++) {
			String srchAspectLine = advice.get(i);
			if(srchAspectLine.matches(replacePattern)) {//replace
				ArrayList<String> connectors = srchConnectorFromAdivce(srchAspectLine);
				String adviceConnector = connectors.get(0);
				String pointcutConnector = connectors.get(1);
				ArrayList<String> weavingInstances = searchInstance(adviceConnector,instances);
				FileIO fileio = new FileIO();
				ArrayList<Integer> indxs = new ArrayList<Integer>();
				for(int j =0;j<weavingInstances.size();j++) {
					String str ="R" + weavingInstances.get(j);//織り込み先のモデルに上書きするためのR
					weavingInstances.set(j, str);
					indxs.add(j);
				}
				weavingInstances = fileio.replaceKeyword(weavingInstances,indxs,pointcutConnector,adviceConnector);
				interpretedAdvice.addAll(weavingInstances);
				//因果的なコネクタの生成及び接続
				interpretedAdvice.add("I:  Modelica.Blocks.Interfaces.RealInput u" +numU+";");// TODO:replaceで必ずセンサが来るとはかぎらんだろ
				String actuatorConn = actCausalConn(weavingInstances,"RealInput");
				interpretedAdvice.add("A:  connect(u"+numU+","+actuatorConn+");");
				numU++;
			} else if(srchAspectLine.matches(addPattern)) {//add
				ArrayList<String> connectors = srchConnectorFromAdivce(srchAspectLine);
				String adviceConnector = connectors.get(0);
				String pointcutConnector = connectors.get(1);
				ArrayList<String> weavingInstances = searchInstance(adviceConnector,instances);
				interpretedAdvice.addAll(weavingInstances);//インスタンス生成文
				interpretedAdvice.add("A:  connect("+adviceConnector+"," +pointcutConnector+");");//connect文
				//因果的なコネクタの生成及び接続
				interpretedAdvice.add("I:  Modelica.Blocks.Interfaces.RealOutput y"+numY+";");// TODO:replaceで必ずアクチュエータが来るとはかぎらんだろ
				String actuatorConn = actCausalConn(weavingInstances,"RealOutput");
				interpretedAdvice.add("A:  connect(y"+numY+"," + actuatorConn+");");
				numY++;
			} else if(srchAspectLine.matches(insertPattern)) {//insert
				//TODO:insertの処理を追加して
			} else if(srchAspectLine.matches(linkPattern)) {//link
				ArrayList<String> connectors = srchConnectorFromAdivce(srchAspectLine);
				String adviceConnector = connectors.get(0);
				String pointcutConnector = connectors.get(1);
				interpretedAdvice.add("A:  connect("+adviceConnector+","+pointcutConnector+");");
			} 
		}
		return interpretedAdvice;
	}
	
	/**
	 * アクチュエータの因果的なコネクタの名前を取得するメソッド
	 * @param weavingInstances 織り込まれるインスタンス生成文列
	 * @param io　RealInputかRealOutputか
	 * @return アクチュエータの因果的なコネクタの名前
	 */
	// TODO:因果的なコネクタが複数存在するアクチュエータの場合はどうするつもりだ
	private String actCausalConn(ArrayList<String> weavingInstances,String io) {
		Scope scp = new Scope();
		FileIO fileio = new FileIO();
		String instanceName="";
		String conn="";
		for(String instance : weavingInstances) {
			instanceName = instance.split("\\s")[3].split(";")[0];
			String path = instance.split("\\s")[2];
			ArrayList<String> model = scp.publicInputModel(path);
			ArrayList<Integer> indxs = fileio.searchKeyword(model,io);
			conn = model.get(indxs.get(0)).split(io+"\\s")[1].split("\\(")[0];
		}
		return (instanceName+"."+conn) ;
	}

	/**
	 * アドバイスのコネクタ指定パスにおいてinstances列と一致するインスタンス名があればそれを返す
	 * @param advicePath アドバイスのコネクタ指定パス
	 * @param instances　アドバイスのインスタンス生成列
	 * @return　アドバイスで指定されたインスタンス生成列
	 */
	private ArrayList<String> searchInstance(String advicePath, ArrayList<String> instances) {
		String adviceInstance = advicePath.split(Pattern.quote("."))[0];
		ArrayList<String> weavingInstances = new ArrayList<String>();
		for(String instance:instances) {
			if(instance.contains(adviceInstance)) {
				weavingInstances.add("I:" + instance);//I:はインスタンス生成領域を表す
			}
		}
		return weavingInstances;
	}

	/**
	 * アドバイス構文からアドバイスに指定されているコネクタとポイントカットで指定されているコネクタをの二つを抽出する
	 * @param advice　アドバイス構文
	 * @return　コネクタ列
	 */
	private ArrayList<String> srchConnectorFromAdivce(String advice) {
		ArrayList<String> connectors = new ArrayList<String>();
		String adviceConnector =  advice.split("\\(")[1];//angleSensor.flange)annotation
		connectors.add(adviceConnector.split("\\)")[0]);//アドバイス内のコネクタを抽出
		String pointcutConnector = advice.split("\"")[1];//* mortor.flange_b
		connectors.add(pointcutConnector.split("\\s")[1]);//ポイントカット内のコネクタを抽出
		
		return connectors;
	}

	/**
	 * 正しい構文が記述されているか調べるメソッド
	 * syntaxCheck:構文の正しさを調べる
	 * searchKeyword:ファイルにあるキーワードが存在するか調べる
	 * @param arrList　調べる対象
	 * @param pattern　正しい構文を表す正規表現
	 * @return　正しい構文列
	 */
	public ArrayList<String> syntaxCheck(ArrayList<String> arrList, String pattern) {
		ArrayList<String> rightArrList = new ArrayList<String>();
		for(String str : arrList) {
			if(str.matches(pattern)) {
				rightArrList.add(str);
			}
		}
		return rightArrList;
	}

	/**
	 * 探索構文から非因果制御対象の名前を抜き出す(ConvertAspect用のメソッド）
	 * @param srchSyntaxes 探索構文列
	 * @return　非因果制御対象モデル名
	 */
	public String takeNonCausalModelName(ArrayList<String> srchSyntaxes) {
		String result ="";
		for(String syntax : srchSyntaxes) {
			String modelName = syntax.split("\"")[1].split("\\s")[0];
			if(takeResult.equals("")) {
				takeResult = modelName;
			}  else if(modelName.equals("*")) {
				//*はどのモデルを指定しているか明確でないので無視
			} else if (!takeResult.equals(modelName)) {
				//TODO: 複数の非因果制御対象が存在した場合の処理を追加してください
				System.out.println("複数の制御対象モデルには対応しておりません");
			}
		}
		if(takeResult.equals(""))System.out.println("非因果制御対象の名前は空です:InterpreterクラスtakeNonCausalModelName");
		return takeResult;
	}
	
}
