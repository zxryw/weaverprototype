model DCmortorNONCAUSAL
  Modelica.Electrical.Analog.Basic.Resistor R annotation(
    Placement(visible = true, transformation(origin = {-50, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Inductor L annotation(
    Placement(visible = true, transformation(origin = {-10, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Capacitor C annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Basic.Ground ground1 annotation(
    Placement(visible = true, transformation(origin = {-40, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.EMF emf annotation(
    Placement(visible = true, transformation(origin = {22, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Inertia mortor annotation(
    Placement(visible = true, transformation(origin = {50, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.StepVoltage stepVoltage1 annotation(
    Placement(visible = true, transformation(origin = {-68, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
equation
  connect(stepVoltage1.p, ground1.p) annotation(
    Line(points = {{-68, -10}, {-68, -20}, {-40, -20}}, color = {0, 0, 255}));
  connect(stepVoltage1.n, R.p) annotation(
    Line(points = {{-68, 10}, {-68, 20}, {-60, 20}}, color = {0, 0, 255}));
  connect(C.p, emf.p) annotation(
    Line(points = {{0, 10}, {22, 10}}, color = {0, 0, 255}));
  connect(C.n, emf.n) annotation(
    Line(points = {{0, -10}, {22, -10}}, color = {0, 0, 255}));
  connect(emf.flange, mortor.flange_a) annotation(
    Line(points = {{32, 0}, {40, 0}}));
  connect(L.n, C.p) annotation(
    Line(points = {{0, 20}, {0, 10}}, color = {0, 0, 255}));
  connect(R.n, L.p) annotation(
    Line(points = {{-40, 20}, {-20, 20}}, color = {0, 0, 255}));
  connect(C.n, ground1.p) annotation(
    Line(points = {{0, -10}, {0, -10}, {0, -20}, {-40, -20}, {-40, -20}}, color = {0, 0, 255}));
  annotation(
    uses(Modelica(version = "3.2.2")),
    Icon(graphics = {Rectangle(origin = {84, 0}, extent = {{26, 10}, {-26, -10}}), Rectangle(origin = {-21, 0}, extent = {{-79, 60}, {79, -60}})}));
end DCmortorNONCAUSAL;
