package weaver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * ファイルの入出力やファイル内部の解析を行うクラス
 * @author watanabe
 *
 */
public class FileIO {

	
	/**
	 * 指定された名前のファイルをArrayListとして読み込む
	 * @param filename 読み込みたいファイル名
	 * @return　読み込んだファイル（ArrayList)
	 */
	public ArrayList<String> input(String filename) {
		File file = new File(filename);
		String line = "";
		ArrayList<String> inputResult = new ArrayList<>();
	
		if(file.exists()) {
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
				while((line = br.readLine()) != null){
					inputResult.add(line);
				}
				br.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else {
			System.out.println(filename+":File not open");
			inputResult.add(filename+":File not open");
		}
		return inputResult;
	}

	/**
	 * あるArrayListを入力としてファイルを出力するメソッド
	 * @param inputarr 出力したいファイル列
	 * @param nfilename　出力ファイルの名前
	 * @return　出力した結果
	 */
	public ArrayList<String> output(ArrayList<String> inputarr,String nfilename) {
		File newfile = new File(nfilename);
		ArrayList<String> outputResult = new ArrayList<>();
		
		try {
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newfile),"UTF-8"));
			
			for(int i=0;i<inputarr.size();i++) {
				bw.write(inputarr.get(i));
				bw.newLine();
				outputResult.add(inputarr.get(i));
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return outputResult;
	}
	
	/**
	 * ArrayListに対して指定したインデックスに文字列を差し込む
	 * @param arrIn 対象
	 * @param overWrtStr 追加したい文字列
	 * @param indx 文字列が追加されるインデックス
	 * @return 文字列が挿入されたArrayList
	 */
	public ArrayList<String> writeString(ArrayList<String> arrIn, String overWrtStr, int indx) {
		int addStrs = 1;
		int max = (arrIn.size()-1)+addStrs;//最終インデックス＋追加文字列の数
		
		for(int i =0;i<addStrs;i++){
			arrIn.add("**this is dummy**");
		}
		
		for(int i = max;indx<i;i--) {
			String tmp = arrIn.get(i-1);
			arrIn.set(i,tmp);
		}
		
		arrIn.set(indx, overWrtStr);
		return arrIn;
	}

	/**
	 * 与えられた文字列に部分一致したインデックスを返す
	 * @param arrlist 探索列
	 * @param keyword　探索ワード
	 * @return 一致したインデックスのリストを返す
	 */
	public ArrayList<Integer> searchKeyword(ArrayList<String> arrlist, String keyword) {
		ArrayList<Integer> indxs = new ArrayList<Integer>();
		
		for(int i=0;i<arrlist.size();i++) {
			if(arrlist.get(i).contains(keyword)) {
				indxs.add(i);
			}
		}
		if(indxs.size() == 0) {
			indxs.add(-1);
			System.out.println(arrlist.get(0)+"::::"+keyword + ":::searchKeywordメソッドにおいて検索に失敗した可能性があります");
		}
		return indxs;//指定した文字列が存在する行を返す
	}
	
	
	/**
	 * ArrayListから特定のキーワードを探し出しそれを別のキーワードに置換する
	 * @param arrlist 対象列
	 * @param indxs　キーワードが存在するインデックス
	 * @param replacement　変換するキーワード
	 * @param keyword　変換されるキーワード
	 * @return　結果
	 */
	public ArrayList<String> replaceKeyword(ArrayList<String> arrlist, ArrayList<Integer> indxs, String replacement, String keyword) {
		for(int i=0;i<indxs.size();i++) {
			String result = arrlist.get(indxs.get(i)).replace(keyword, replacement);
			arrlist.set(indxs.get(i),result);
		}
		return arrlist;
	}	
}
